#include <QAction>
#include <QFileDialog>
#include <QMenuBar>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include "editor.h"

TEditor::TEditor(QWidget *parent)
    : QMainWindow(parent)
{
    setGeometry(20, 20, 2000, 1000);
    data = new TDiagramsData(this);
    codeField = new TCodeField(data, this);
    diagramsTree = new TDiagramsTree(data, this);
    CreateMenus();
    CreateLayout();
    setWindowTitle(tr("DRAKON-C Edtior"));

}

TEditor::~TEditor() {
    delete insertFolder;
    delete insertDiagram;
    delete data;
    delete codeField;
    delete diagramsTree;
}

void TEditor::CreateMenus() {
    //create file menu
    fileMenu = menuBar()->addMenu(tr("File"));

    newAct = new QAction(tr("New..."), this);
    fileMenu->addAction(newAct);
    connect(newAct, SIGNAL(triggered()), data, SLOT(NewFile()));


    openAct = new QAction(tr("Open..."), this);
    fileMenu->addAction(openAct);
    connect(openAct, SIGNAL(triggered()), data, SLOT(OpenFile()));

    saveAct = new QAction(tr("Save..."), this);
    fileMenu->addAction(saveAct);
    connect(saveAct, SIGNAL(triggered()), data, SLOT(SaveFile()));

    fileMenu->addSeparator();

    quitAct = new QAction(tr("Quit"), this);
    fileMenu->addAction(quitAct);
    connect(quitAct, SIGNAL(triggered()), this, SLOT(close()));

    //create edit menu
    editMenu = menuBar()->addMenu(tr("Edit"));

    undoAct = new QAction(tr("Undo"), this);
    undoAct->setShortcut(QKeySequence::Undo);
    editMenu->addAction(undoAct);
    connect(undoAct, SIGNAL(triggered()), this, SLOT(Undo()));

    redoAct = new QAction(tr("Redo"), this);
    redoAct->setShortcut(QKeySequence::Redo);
    editMenu->addAction(redoAct);
    connect(redoAct, SIGNAL(triggered()), this, SLOT(Redo()));
    editMenu->addSeparator();

    copyAct = new QAction(tr("Copy"), this);
    copyAct->setShortcut(QKeySequence::Copy);
    editMenu->addAction(copyAct);
    connect(copyAct, SIGNAL(triggered()), this, SLOT(Copy()));

    cutAct = new QAction(tr("Cut"), this);
    cutAct->setShortcut(QKeySequence::Cut);
    editMenu->addAction(cutAct);
    connect(cutAct, SIGNAL(triggered()), this, SLOT(Cut()));

    pasteAct = new QAction(tr("Paste"), this);
    pasteAct->setShortcut(QKeySequence::Paste);
    editMenu->addAction(pasteAct);
    connect(pasteAct, SIGNAL(triggered()), this, SLOT(Paste()));

    deleteAct = new QAction(tr("Delete"), this);
    deleteAct->setShortcut(QKeySequence::Delete);
    editMenu->addAction(deleteAct);
    connect(deleteAct, SIGNAL(triggered()), this, SLOT(Delete()));
    codeField->addAction(deleteAct);
    connect(deleteAct, SIGNAL(triggered()), codeField, SLOT(DoDelete()));
    diagramsTree->addAction(deleteAct);
    connect(deleteAct, SIGNAL(triggered()), diagramsTree, SLOT(DoDelete()));

    editMenu->addSeparator();

    diagramDescriptionAct = new QAction(tr("Diagram description..."), this);
    diagramDescriptionAct->setShortcut(QKeySequence(tr("Ctrl+D")));
    editMenu->addAction(diagramDescriptionAct);
    connect(diagramDescriptionAct, SIGNAL(triggered()), this, SLOT(DiagramDescription()));

    selectAllAct = new QAction(tr("Select all"), this);
    selectAllAct->setShortcut(QKeySequence::SelectAll);
    editMenu->addAction(selectAllAct);
    connect(selectAllAct, SIGNAL(triggered()), this, SLOT(SelectAll()));

    editMenu->addSeparator();

    findAct = new QAction(tr("Find"), this);
    findAct->setShortcut(QKeySequence::Find);
    editMenu->addAction(findAct);
    connect(findAct, SIGNAL(triggered()), this, SLOT(Find()));

    gotoDiagramAct = new QAction(tr("Go to diagram..."), this);
    gotoDiagramAct->setShortcut(QKeySequence(tr("Ctrl+G")));
    editMenu->addAction(gotoDiagramAct);
    connect(gotoDiagramAct, SIGNAL(triggered()), this, SLOT(GotoDiagram()));

    //create insert menu
    insertMenu = menuBar()->addMenu(tr("Insert"));

    insertDgrmAct = new QAction(tr("Insert diagram..."), this);
    insertDgrmAct->setShortcut(QKeySequence(tr("Ctrl+N")));
    insertMenu->addAction(insertDgrmAct);
    insertDiagram = new TTextEdit();
    insertDiagram->setWindowTitle(tr("Create new diagram"));
    connect(insertDgrmAct, SIGNAL(triggered()), insertDiagram, SLOT(show()));
    connect(insertDiagram, SIGNAL(TextOk()), this, SLOT(InsertDgrm()));
    connect(insertDiagram, SIGNAL(TextCancel()), insertDiagram, SLOT(hide()));

    insertFolderAct = new QAction(tr("Insert folder..."), this);
    insertMenu->addAction(insertFolderAct);
    insertFolder = new TTextEdit();
    insertFolder->setWindowTitle(tr("Create new folder"));
    connect(insertFolderAct, SIGNAL(triggered()), insertFolder, SLOT(show()));
    connect(insertFolder, SIGNAL(TextOk()), this, SLOT(InsertFolder()));
    connect(insertFolder, SIGNAL(TextCancel()), insertFolder, SLOT(hide()));

    //create drakon menu
    drakonMenu = menuBar()->addMenu(tr("DRAKON"));

    verifyAct = new QAction(tr("Verify"), this);
    drakonMenu->addAction(verifyAct);
    connect(verifyAct, SIGNAL(triggered()), data, SLOT(VerifyCurrent()));

    verifyAllAct = new QAction(tr("Verify All"), this);
    drakonMenu->addAction(verifyAllAct);
    connect(verifyAllAct, SIGNAL(triggered()), data, SLOT(VerifyAll()));

    drakonMenu->addSeparator();

    buildAct = new QAction(tr("Build"), this);
    buildAct->setShortcut(QKeySequence(tr("Ctrl+B")));
    drakonMenu->addAction(buildAct);
    connect(buildAct, SIGNAL(triggered()), this, SLOT(BuildCode()));

    runAct = new QAction(tr("Run"), this);
    runAct->setShortcut(QKeySequence(tr("Ctrl+R")));
    drakonMenu->addAction(runAct);
    connect(runAct, SIGNAL(triggered()), this, SLOT(RunCode()));

}

void TEditor::CreateLayout() {
    setCentralWidget(codeField);
    QDockWidget *fileView = new QDockWidget(tr("Files"), this);
    fileView->setFeatures(QDockWidget::NoDockWidgetFeatures);
    fileView->setWidget(diagramsTree);
    addDockWidget(Qt::DockWidgetArea::LeftDockWidgetArea, fileView);
    /*descriptionField = new QTextEdit();
    descriptionField->setText("Description Field");
    QDockWidget *descriptionView = new QDockWidget(tr("Description"), this);
    descriptionView->setFeatures(QDockWidget::NoDockWidgetFeatures);
    descriptionView->setWidget(descriptionField);
    addDockWidget(Qt::DockWidgetArea::RightDockWidgetArea, descriptionView);*/

}

void TEditor::Undo() {
}

void TEditor::Redo() {

}

void TEditor::Cut() {

}

void TEditor::Copy() {

}

void TEditor::Paste() {

}

void TEditor::Find() {

}

void TEditor::SelectAll() {

}

void TEditor::Delete() {

}

void TEditor::DiagramDescription() {

}

void TEditor::GotoDiagram() {

}

void TEditor::InsertDgrm() {
   QString name;
   insertDiagram->GetText(name);
   insertDiagram->SetText("");
   if (name.isEmpty()) {
       QMessageBox::warning(this, "", "Name is empty");
       return;
   }
   insertDiagram->hide();
   QString p = "";
   if (diagramsTree->currentItem()) {
       p = diagramsTree->currentItem()->text(0);
   }
   data->AddNewDiagram(name, p);
}

void TEditor::InsertFolder() {
    QString name;
    insertFolder->GetText(name);
    insertFolder->SetText("");
    if (name.isEmpty()) {
        QMessageBox::warning(this, "", "Name is empty");
        return;
    }
    insertFolder->hide();
    QString p = "";
    if (diagramsTree->currentItem()) {
        p = diagramsTree->currentItem()->text(0);
    }
    data->AddNewFolder(name, p);
}

void TEditor::VerifyCode() {

}

void TEditor::VerifyAllCode() {

}

void TEditor::BuildCode() {

}

void TEditor::RunCode() {

}
