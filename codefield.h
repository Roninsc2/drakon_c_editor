#pragma once

#include <QWidget>
#include <QTextEdit>
#include <QLayout>
#include <QGraphicsScene>
#include <QGraphicsView>
#include "insertmenu.h"
#include "data/diagramsdata.h"
#include <QUndoStack>

#define MOVE_FIELD 10

class TCodeField : public QWidget
{
    Q_OBJECT
public:
    explicit TCodeField(TDiagramsData *data, QWidget *parent = nullptr);
    ~TCodeField();
protected:
    void mousePressEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
private slots:
    void MoveDown();
    void MoveUp();
    void MoveLeft();
    void MoveRight();
    void PullDiagramData();
    void AddNewActionDiagram();
    void AddNewBeginEndDiagram();
    void AddNewVerticalDiagram();
    void AddNewHorizontalDiagram();
    void AddNewIfElseDiagram();
public slots:
    void DoUndo();
    void DoCut();
    void DoCopy();
    void DoPaste();
    void DoDelete();
private:
    void SetupFiledMenu();
private:
    QAction *moveDown;
    QAction *moveUp;
    QAction *moveRight;
    QAction *moveLeft;
    QGraphicsScene *scene;
    QGraphicsView *viewField;
    QLayout * layout;
    TCodeFieldMenu *fieldMenu;
    TDiagramsData *data;
};
