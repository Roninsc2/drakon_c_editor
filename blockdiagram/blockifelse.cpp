#include "blockifelse.h"
#include <QPainter>
#include <QFont>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QKeyEvent>

TBlockIfElse::TBlockIfElse(TBlockDiagram*parent)
    : TBlockDiagram(parent)
{
    id = 0;
    diagramId = 0;
    blockW = 150;
    blockH = 100;
    setPos(-blockW/2, -blockH/2);
    a = MIN_WIDTH;
    b = 0;
    right = "Yes";
    down = "No";
    blockType = TIfElse;
    textEdit = new TTextEdit(true);
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    connect(textEdit, SIGNAL(SwapIfElse()), this, SLOT(SwapIfElse()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    connectPoints.resize(3);
    setZValue(TBlockType::TIfElse);
}

TBlockIfElse::TBlockIfElse(int id, int dId, TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    this->id = id;
    diagramId = dId;
    blockW = 150;
    blockH = 100;
    setPos(-blockW/2, -blockH/2);
    a = MIN_WIDTH;
    b = 0;
    right = "Yes";
    down = "No";
    blockType = TIfElse;
    textEdit = new TTextEdit(true);
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    connect(textEdit, SIGNAL(SwapIfElse()), this, SLOT(SwapIfElse()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    connectPoints.resize(3);
    setZValue(TBlockType::TIfElse);
}

TBlockIfElse::TBlockIfElse(int id, int dId, QString text,
                           int xPos, int yPos, int w, int h, int b)
{
    blockType = TIfElse;
    textEdit = new TTextEdit(true);
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    connect(textEdit, SIGNAL(SwapIfElse()), this, SLOT(SwapIfElse()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    a = MIN_WIDTH;
    this->b = b;
    if (b) {
        right = "No";
        down = "Yes";
    } else {
        right = "Yes";
        down = "No";
    }
    this->id = id;
    diagramId = dId;
    this->text = text;
    setPos(xPos - w, yPos - h);
    blockW = w*2;
    blockH = h*2;
    connectPoints.resize(3);
    setZValue(TBlockType::TIfElse);
}

TBlockIfElse::~TBlockIfElse() {
    delete  textEdit;
}

void TBlockIfElse::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *parent) {
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
    QFont font = painter->font();
    font.setPixelSize(FONT_SIZE);
    painter->setFont(font);
    QPen pen(Qt::black);
    pen.setWidth(PEN_WIDTH);
    QBrush brush(Qt::white);
    painter->setBrush(brush);
    painter->setPen(pen);
    QPolygon pol;
    pol << QPoint(0, blockH/2) << QPoint(MIN_WIDTH/2, 0) << QPoint(blockW-MIN_WIDTH/2, 0);
    pol << QPoint(blockW, blockH/2) << QPoint(blockW + a, blockH/2) << QPoint(blockW, blockH/2);
    pol << QPoint(blockW-MIN_WIDTH/2, blockH) << QPoint(MIN_WIDTH/2, blockH) << QPoint(0, blockH/2);
    painter->drawPolygon(pol);
    painter->drawText(blockW, blockH/2-FONT_SIZE/2, right);
    painter->drawText(blockW/2-MIN_WIDTH, blockH+FONT_SIZE, down);
    painter->drawText(QRect(0, 0, blockW, blockH), Qt::AlignCenter, text);
    if (isSelected()) {
        brush.setColor(Qt::green);
        pen.setWidth(PEN_WIDTH/2);
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(resizeRectX, resizeRectY, resizeRectWidth, resizeRectHeight);
    }
}

QRectF TBlockIfElse::boundingRect() const {
    return QRectF(- PEN_WIDTH / 2 + resizeRectX,
                  - PEN_WIDTH / 2 + resizeRectY - FONT_SIZE/2,
                  blockW + PEN_WIDTH + resizeRectWidth + a,
                  blockH + PEN_WIDTH + resizeRectHeight + FONT_SIZE);
}

QPolygon TBlockIfElse::GetPolygon() const {
    QPolygon pol;
    int x = pos().x();
    int y = pos().y();
    pol << QPoint(x, y) << QPoint(x + blockW + a, y) << QPoint(x + blockW + a, y+blockH);
    pol << QPoint(x, y+blockH) <<  QPoint(x, y);
    return pol;
}

int TBlockIfElse::GetX() {
    return x() + blockW/2;
}

int TBlockIfElse::GetY() {
    return y() + blockH/2;
}

int TBlockIfElse::GetWidth() {
    return blockW/2;
}

int TBlockIfElse::GetHeight() {
    return blockH/2;
}

int TBlockIfElse::GetA() {
    return a;
}

int TBlockIfElse::GetB() {
    return b;
}

void TBlockIfElse::UpdatePos(QPoint delta) {
    setPos(pos() + delta);
    update();
}

void TBlockIfElse::AddBlock(TBlockDiagram *other) {
    if (other->GetType() == TBlockType::TVerticalLine) {
        verticalLine = other;
    }
    if (other->GetType() == TBlockType::THorizontalLine) {
        horizontalLine = other;
    }
}

void TBlockIfElse::ClearBlockConnections() {
    if (verticalLine) {
        verticalLine->RemoveBlockConnections(this);
    }
    if (horizontalLine) {
        horizontalLine->RemoveBlockConnections(this);
    }
    verticalLine = nullptr;
    horizontalLine = nullptr;
}

void TBlockIfElse::RemoveBlockConnections(TBlockDiagram *other) {
    if (other == verticalLine) {
        verticalLine = nullptr;
    }
    if (other == horizontalLine) {
        horizontalLine = nullptr;
    }
}

QVector<QPoint> *TBlockIfElse::GetConnectPoints() {
    connectPoints[0] = QPoint(x()+blockW/2, y());
    connectPoints[1] = QPoint(x()+blockW/2, y()+blockH);
    connectPoints[2] = QPoint(x()+blockW + a, y()+blockH/2);
    return &connectPoints;
}

void TBlockIfElse::Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) {
    if (alreadyMove.contains(id)) {
        return;
    }
    alreadyMove.insert(id);
    if (int(y()) >= leftTop.y()) {
        setY(y() + move.y());
    }
    if (int(x()) >= leftTop.x()) {
        setX(x() + move.x());
    }
    update();
    if (verticalLine && !alreadyMove.contains(verticalLine->GetItemId())) {
        verticalLine->Move(leftTop, move, alreadyMove);
    }
    if (horizontalLine && !alreadyMove.contains(horizontalLine->GetItemId())) {
        horizontalLine->Move(leftTop, move, alreadyMove);
    }
}

TError TBlockIfElse::Verify(QVector<TBlockDiagram *> intersects) {
    if (!verticalLine || !horizontalLine) {
        return VerifyIfEsleErrorConnToLine;
    }
    if (intersects.size() != 2) {
        return VerifyIfEsleErrorIntersectsSize;
    }
    for (int i = 0; i < intersects.size(); i++) {
        if (intersects.at(i) != verticalLine && intersects.at(i) != horizontalLine) {
            return VerifyIfEsleErrorIntersectsLines;
        }
    }
    return Ok;
}

void TBlockIfElse::TextOk() {
    int oldW = blockW/2;
    textEdit->GetText(text);
    textEdit->hide();
    blockH = MIN_HEIGHT;
    blockW = MIN_WIDTH;
    int w = 0;
    int h = 1;
    int maxW = 0;
    int maxH = 1;
    for (int i = 0; i < text.length(); i++) {
        w++;
        if (text.at(i) == '\n') {
            h++;
            if (maxW < w) {
                maxW = w;
            }
            w = 0;
        }
    }
    if (maxW < w) {
        maxW = w;
    }
    if (maxH < h) {
        maxH = h;
    }
    maxH *= HEIGHT_TEXT_SIZE;
    maxW *= WIDTH_TEXT_SIZE;
    if (maxH > MIN_HEIGHT) {
        blockH = maxH;
    }
    if (maxW > MIN_WIDTH) {
        blockW = maxW;
    }
    blockH += FONT_SIZE;
    blockW += FONT_SIZE;
    setX(x() + (oldW - blockW/2));
    update();
    emit PosChanged();
}

void TBlockIfElse::TextCancel() {
    textEdit->SetText(text);
    textEdit->hide();
}

void TBlockIfElse::SwapIfElse() {
    if (b) {
        b = 0;
        right = "Yes";
        down = "No";
        return;
    }
    b = 1;
    right = "No";
    down = "Yes";
}

void TBlockIfElse::mousePressEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        resizeFlag = (e->pos().x() >= resizeRectX)
                && (e->pos().x() <= (resizeRectX + resizeRectWidth))
                && (e->pos().y() >= resizeRectY)
                && (e->pos().y() <= (resizeRectY + resizeRectHeight));
    }
    QGraphicsObject::mousePressEvent(e);
}

void TBlockIfElse::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        textEdit->SetText(text);
        textEdit->show();
    }
    QGraphicsObject::mouseDoubleClickEvent(e);
}

void TBlockIfElse::mouseMoveEvent(QGraphicsSceneMouseEvent *e) {
    if (resizeFlag) {
        prepareGeometryChange();
        blockW -= int(e->pos().x());
        blockH -= int(e->pos().y());
        if (blockW < MIN_WIDTH) {
            blockW = MIN_WIDTH;
        }
        if (blockH < MIN_HEIGHT) {
            blockH = MIN_HEIGHT;
        }
        setX(x() + int(e->pos().x()));
        setY(y() + int(e->pos().y()));
    } else {
        QGraphicsObject::mouseMoveEvent(e);
    }
    update();
}

void TBlockIfElse::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
    resizeFlag = false;
    QGraphicsObject::mouseReleaseEvent(e);
    update();
    emit PosChanged();
}
