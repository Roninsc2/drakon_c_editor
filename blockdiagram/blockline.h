#pragma once
#include "blockdiagram.h"
#include <QGraphicsObject>
#include "textedit.h"
#include <QSet>

class TBlockVerticalLine: public TBlockDiagram
{
    Q_OBJECT

public:
    TBlockVerticalLine(TBlockDiagram *parent = nullptr);
    TBlockVerticalLine(int id, int dId, TBlockDiagram *parent = nullptr);
    TBlockVerticalLine(int id, int dId, int x, int y, int h);
    ~TBlockVerticalLine() override;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget *parent = nullptr) override;
    QRectF boundingRect() const override;
    QPolygon GetPolygon() const override;
    int getMinResizeHeight() const;
    int GetX() override;
    int GetY() override;
    int GetWidth() override;
    int GetHeight() override;
    int GetA() override;
    int GetB() override;
    void UpdatePos(QPoint delta) override;
    void AddBlock(TBlockDiagram *other) override;
    void ClearBlockConnections() override;
    void RemoveBlockConnections(TBlockDiagram *other) override;
    QVector<QPoint>* GetConnectPoints() override;
    void Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) override;
    TError Verify(QVector<TBlockDiagram *> intersects) override;
private:
    void CheckForMove();
    void UpdateVerifyCount(int &count, const TBlockType type, QVector<QPoint> *otherPoints);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
private:
    QSet<TBlockDiagram*> connectedBlocks;
    const int resizeRectWidth = 30;
    const int resizeRectHeight = 30;
    int resizeRectX = -15;
    int resizeRectY = -15;
    bool resizeFlagTop = false;
    bool resizeFlagBot = false;
};

class TBlockHorizontalLine: public TBlockDiagram
{

    Q_OBJECT
public:
    TBlockHorizontalLine(TBlockDiagram *parent = nullptr);
    TBlockHorizontalLine(int id, int dId, TBlockDiagram *parent = nullptr);
    TBlockHorizontalLine(int id, int dId, int blockX, int blockY, int blockW);
    ~TBlockHorizontalLine() override;
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget *parent = nullptr)  override;
    QRectF boundingRect() const override;
    QPolygon GetPolygon() const override;
    int GetX() override;
    int GetY() override;
    int GetWidth() override;
    int GetHeight() override;
    int GetA() override;
    int GetB() override;
    void UpdatePos(QPoint delta) override;
    void AddBlock(TBlockDiagram *other) override;
    void ClearBlockConnections() override;
    void RemoveBlockConnections(TBlockDiagram *other) override;
    QVector<QPoint>* GetConnectPoints() override;
    void Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) override;
    TError Verify(QVector<TBlockDiagram *> intersects) override;
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event)  override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)  override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)  override;
private:
    void UpdateVerifyCount(int &count, const TBlockType type, QVector<QPoint> *otherPoints);
    void CheckForMove();
private:
    QSet<TBlockDiagram*> connectedBlocks;
    const int resizeRectWidth = 30;
    const int resizeRectHeight = 30;
    int resizeRectX = -15;
    int resizeRectY = -15;
    bool resizeFlagLeft = false;
    bool resizeFlagRight = false;
};
