#pragma once
#include <QString>
#include <QVector>
#include "blockdiagram.h"

class TDiagram : public QObject
{
    Q_OBJECT
public:
    TDiagram();
    TDiagram(int id, QString &name, QString descr, double zoom = 1.0);
    ~TDiagram();   
public:
    QString GetName();
    int GetId();
    QString GetDescription();
    double GetZoom();
    void SetZoom(double z);
    void InsertBlock(TBlockDiagram *block, int &maxItemId);
    void InsertBlock(TBlockDiagram *block);
    void RemoveBlock(TBlockDiagram *block);
    void Clear();
    QVector<TBlockDiagram*> *GetBlocks();
    TError Verify();
private slots:
    void UpdateBlockConnections();
private:
    void CreateBlockConnection(TBlockDiagram *block);
    void CreateBlocksConnections(TBlockDiagram* updated, TBlockDiagram* other);
    void ConnectBlocks(TBlockDiagram *first, TBlockDiagram *second);
    void SetupMapConnectionPoints();
private:
    int id;
    double zoom = 1.0;
    QString name;
    QString descr;
    //get points for first in Pair
    QMap<QPair<TBlockType, TBlockType>, QVector<int>> connectionPoints;
    QVector<TBlockDiagram*> items;
};
