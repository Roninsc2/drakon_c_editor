#include "blockaction.h"
#include <QPainter>
#include <QFont>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QKeyEvent>

TBlockAction::TBlockAction(TBlockDiagram*parent)
    : TBlockDiagram(parent)
{
    id = 0;
    diagramId = 0;
    blockW = 150;
    blockH = 100;
    setPos(-blockW/2, -blockH/2);
    a = 0;
    b = 0;
    blockType = TAction;
    textEdit = new TTextEdit();
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    connectPoints.resize(3);
    setZValue(TBlockType::TAction);
}

TBlockAction::TBlockAction(int id, int dId, TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    this->id = id;
    diagramId = dId;
    blockW = 150;
    blockH = 100;
    setPos(-blockW/2, -blockH/2);
    a = 0;
    b = 0;
    blockType = TAction;
    textEdit = new TTextEdit();
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    connectPoints.resize(3);
    setZValue(TBlockType::TAction);
}

TBlockAction::TBlockAction(int id, int dId, QString text,
                           int xPos, int yPos, int w, int h)
{
    blockType = TAction;
    textEdit = new TTextEdit();
    textEdit->setWindowTitle(tr("Text Editor"));
    connect(textEdit, SIGNAL(TextOk()), this, SLOT(TextOk()));
    connect(textEdit, SIGNAL(TextCancel()), this, SLOT(TextCancel()));
    textEdit->hide();
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    a = 0;
    b = 0;
    this->id = id;
    diagramId = dId;
    this->text = text;
    setPos(xPos - w, yPos - h);
    blockW = w*2;
    blockH = h*2;
    connectPoints.resize(3);
    setZValue(TBlockType::TAction);
}

TBlockAction::~TBlockAction() {
    delete  textEdit;
}

void TBlockAction::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *parent) {
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
    QFont font = painter->font();
    font.setPixelSize(FONT_SIZE);
    painter->setFont(font);
    QPen pen(Qt::black);
    pen.setWidth(PEN_WIDTH);
    QBrush brush(Qt::white);
    painter->setBrush(brush);
    painter->setPen(pen);
    painter->drawRect(0, 0, blockW, blockH);
    painter->drawText(QRect(FONT_SIZE/2, FONT_SIZE/2, blockW-FONT_SIZE/2, blockH-FONT_SIZE/2), Qt::AlignLeft, text);
    if (isSelected()) {
        brush.setColor(Qt::green);
        pen.setWidth(PEN_WIDTH/2);
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(resizeRectX, resizeRectY, resizeRectWidth, resizeRectHeight);
    }
}

QRectF TBlockAction::boundingRect() const {
    return QRectF(- PEN_WIDTH / 2 + resizeRectX,
                  - PEN_WIDTH / 2 + resizeRectY,
                  blockW + PEN_WIDTH + resizeRectWidth/2,
                  blockH + PEN_WIDTH + resizeRectHeight/2);
}

QPolygon TBlockAction::GetPolygon() const {
    QPolygon pol;
    int x = pos().x();
    int y = pos().y();
    pol << QPoint(x, y) << QPoint(x + blockW, y);
    pol << QPoint(x + blockW, y + blockH) << QPoint(x, y + blockH);
    pol << QPoint(x, y);
    return pol;
}

int TBlockAction::GetX() {
    return x() + blockW/2;
}

int TBlockAction::GetY() {
    return y() + blockH/2;
}

int TBlockAction::GetWidth() {
    return blockW/2;
}

int TBlockAction::GetHeight() {
    return blockH/2;
}

int TBlockAction::GetA() {
    return a;
}

int TBlockAction::GetB() {
    return b;
}

void TBlockAction::UpdatePos(QPoint delta) {
    setPos(pos() + delta);
    update();
}

void TBlockAction::AddBlock(TBlockDiagram *other) {
    if (other->GetType() == TBlockType::TVerticalLine) {
        verticalLine = other;
    }
    if (other->GetType() == TBlockType::THorizontalLine) {
        horizontalLine = other;
    }
}

void TBlockAction::ClearBlockConnections() {
    if (verticalLine) {
        verticalLine->RemoveBlockConnections(this);
    }
    if (horizontalLine) {
        horizontalLine->RemoveBlockConnections(this);
    }
    verticalLine = nullptr;
    horizontalLine = nullptr;
}

void TBlockAction::RemoveBlockConnections(TBlockDiagram *other) {
    if (other == verticalLine) {
        verticalLine = nullptr;
    }
    if (other == horizontalLine) {
        horizontalLine = nullptr;
    }
}

QVector<QPoint> *TBlockAction::GetConnectPoints() {
    connectPoints[0] = QPoint(x()+blockW/2, y());
    connectPoints[1] = QPoint(x()+blockW/2, y()+blockH);
    connectPoints[2] = QPoint(x(), y()+blockH/2);
    return &connectPoints;
}

void TBlockAction::Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) {
    if (alreadyMove.contains(id)) {
        return;
    }
    alreadyMove.insert(id);
    if (int(y()) >= leftTop.y()) {
        setY(y() + move.y());
    }
    if (int(x()) >= leftTop.x()) {
        setX(x() + move.x());
    }
    update();
    if (verticalLine && !alreadyMove.contains(verticalLine->GetItemId())) {
        verticalLine->Move(leftTop, move, alreadyMove);
    }
    if (horizontalLine && !alreadyMove.contains(horizontalLine->GetItemId())) {
        horizontalLine->Move(leftTop, move, alreadyMove);
    }
}

TError TBlockAction::Verify(QVector<TBlockDiagram *> intersects) {
    if (horizontalLine && verticalLine) {
        return VerifyActionErrorConnToTwoLines;
    }
    if (!horizontalLine && !verticalLine) {
        return VerifyActionErrorConnToZeroLines;
    }
    if (intersects.size() != 1) {
        return VerifyActionErrorIntersectsSize;
    }
    if (horizontalLine && horizontalLine != intersects.at(0)) {
        return VerifyActionErrorIntersectsHorizontal;
    }
    if (verticalLine && verticalLine != intersects.at(0)) {
        return VerifyActionErrorIntersectsVertical;
    }
    return Ok;
}

void TBlockAction::TextOk() {
    int oldW = blockW/2;
    textEdit->GetText(text);
    textEdit->hide();
    blockH = MIN_HEIGHT;
    blockW = MIN_WIDTH;
    int w = 0;
    int h = 1;
    int maxW = 0;
    int maxH = 1;
    for (int i = 0; i < text.length(); i++) {
        w++;
        if (text.at(i) == '\n') {
            h++;
            if (maxW < w) {
                maxW = w;
            }
            w = 0;
        }
    }
    if (maxW < w) {
        maxW = w;
    }
    if (maxH < h) {
        maxH = h;
    }
    maxH *= HEIGHT_TEXT_SIZE;
    maxW *= WIDTH_TEXT_SIZE;
    if (maxH > MIN_HEIGHT) {
        blockH = maxH;
    }
    if (maxW > MIN_WIDTH) {
        blockW = maxW;
    }
    blockW += FONT_SIZE;
    blockH += FONT_SIZE;
    setX(x() + (oldW - blockW/2));
    update();
    emit PosChanged();
}

void TBlockAction::TextCancel() {
    textEdit->SetText(text);
    textEdit->hide();
}

void TBlockAction::mousePressEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        resizeFlag = (e->pos().x() >= resizeRectX)
                && (e->pos().x() <= (resizeRectX + resizeRectWidth))
                && (e->pos().y() >= resizeRectY)
                && (e->pos().y() <= (resizeRectY + resizeRectHeight));
    }
    QGraphicsObject::mousePressEvent(e);
}

void TBlockAction::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        textEdit->SetText(text);
        textEdit->show();
    }
    QGraphicsObject::mouseDoubleClickEvent(e);
}

void TBlockAction::mouseMoveEvent(QGraphicsSceneMouseEvent *e) {
    if (resizeFlag) {
        prepareGeometryChange();
        blockW -= int(e->pos().x());
        blockH -= int(e->pos().y());
        if (blockW < MIN_WIDTH) {
            blockW = MIN_WIDTH;
        }
        if (blockH < MIN_HEIGHT) {
            blockH = MIN_HEIGHT;
        }
        setX(x() + int(e->pos().x()));
        setY(y() + int(e->pos().y()));
    } else {
        QGraphicsObject::mouseMoveEvent(e);
    }
    update();
}

void TBlockAction::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
   resizeFlag = false;
   QGraphicsObject::mouseReleaseEvent(e);
   update();
   emit PosChanged();
}
