#include "blockline.h"
#include <QPainter>
#include <QFont>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

TBlockVerticalLine::TBlockVerticalLine(TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    id = 0;
    diagramId = 0;
    setPos(0, 0);
    blockH = 300;
    blockW = 0;
    connectPoints.resize(2);
    a = 0;
    b = 0;
    blockType = TVerticalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    setZValue(TBlockType::TVerticalLine);
}

TBlockVerticalLine::TBlockVerticalLine(int id, int dId, TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    this->id = id;
    diagramId = dId;
    setPos(0, 0);
    blockH = 300;
    blockW = 0;
    connectPoints.resize(2);
    a = 0;
    b = 0;
    blockType = TVerticalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    setZValue(TBlockType::TVerticalLine);
}

TBlockVerticalLine::TBlockVerticalLine(int id, int dId, int x, int y, int h) {
    blockType = TVerticalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    this->id = id;
    diagramId = dId;
    blockH = h;
    a = 0;
    b = 0;
    blockW = 0;
    setPos(x, y);
    connectPoints.resize(2);
    setZValue(TBlockType::TVerticalLine);
}

TBlockVerticalLine::~TBlockVerticalLine() {
}

void TBlockVerticalLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *parent) {
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
    QPen pen(Qt::black);
    pen.setWidth(PEN_WIDTH);
    painter->setPen(pen);
    painter->drawLine(0, 0, 0, blockH);
    if (isSelected()) {
        QBrush brush(Qt::green);
        pen.setWidth(PEN_WIDTH/2);
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(resizeRectX, resizeRectY, resizeRectWidth, resizeRectHeight);
        painter->drawRect(resizeRectX, blockH + resizeRectY, resizeRectWidth, resizeRectHeight);
    }
}

QRectF TBlockVerticalLine::boundingRect() const {
    return QRectF(- PEN_WIDTH / 2 + resizeRectX,
                  - PEN_WIDTH / 2 + resizeRectY,
                  PEN_WIDTH + resizeRectWidth,
                  blockH + PEN_WIDTH + resizeRectHeight);
}

QPolygon TBlockVerticalLine::GetPolygon() const {
    QPolygon pol;
    int x = pos().x();
    int y = pos().y();
    pol << QPoint(x, y) << QPoint(x, y+blockH);
    pol << QPoint(x, y);
    return pol;
}

void TBlockVerticalLine::mousePressEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        resizeFlagTop = (e->pos().x() >= resizeRectX)
                && (e->pos().x() <= (resizeRectX + resizeRectWidth))
                && (e->pos().y() >= resizeRectY)
                && (e->pos().y() <= (resizeRectY + resizeRectHeight));
        resizeFlagBot = (e->pos().x() >= resizeRectX)
                && (e->pos().x() <= (resizeRectX + resizeRectWidth))
                && (e->pos().y() >= blockH + resizeRectY)
                && (e->pos().y() <= (blockH + resizeRectY + resizeRectHeight));
    }
    QGraphicsObject::mousePressEvent(e);
}

void TBlockVerticalLine::mouseMoveEvent(QGraphicsSceneMouseEvent *e) {
    if (resizeFlagTop) {
        prepareGeometryChange();
        int delta = e->pos().y();
        if (blockH - delta < MIN_HEIGHT) {
            delta = 0;
        }
        blockH -= delta;
        setY(y() + delta);
    } else if (resizeFlagBot) {
        prepareGeometryChange();
        blockH -= (blockH - e->pos().y());
        if (blockH < MIN_HEIGHT) {
            blockH = MIN_HEIGHT;
        }
    } else {
        QGraphicsObject::mouseMoveEvent(e);
    }
    update();
}

void TBlockVerticalLine::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
    resizeFlagTop = false;
    resizeFlagBot = false;
    QGraphicsObject::mouseReleaseEvent(e);
    update();
    emit PosChanged();
}

int TBlockVerticalLine::GetX() {
    return x();
}

int TBlockVerticalLine::GetY() {
    return y();
}

int TBlockVerticalLine::GetWidth() {
    return blockW;
}

int TBlockVerticalLine::GetHeight() {
    return blockH;
}

int TBlockVerticalLine::GetA() {
    return a;
}

int TBlockVerticalLine::GetB() {
    return b;
}

void TBlockVerticalLine::UpdatePos(QPoint delta) {
    setPos(pos() + delta);
    update();
}

void TBlockVerticalLine::AddBlock(TBlockDiagram *other) {
    connectedBlocks.insert(other);
    CheckForMove();
}


void TBlockVerticalLine::CheckForMove() {
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    QSet<TBlockDiagram*>::iterator jt;
    for (; it != connectedBlocks.end(); it++) {
        TBlockDiagram* a = *it;
        for (jt = it + 1; jt != connectedBlocks.end(); jt++) {
            TBlockDiagram* b = *jt;
            if (a->GetType() != TBlockType::THorizontalLine && b->GetType() != TBlockType::THorizontalLine) {
                QPolygon ap = a->GetPolygon();
                QPolygon bp = b->GetPolygon();
                while (ap.intersects(bp)) {
                    if (ap.at(0).y() == bp.at(0).y()) {
                        b->UpdatePos(QPoint(0, 2));
                        bp = b->GetPolygon();
                    }
                    int topPoint = ap.at(0).y() < bp.at(0).y() ? bp.at(0).y() : ap.at(0).y();
                    QPolygon inter = ap.intersected(bp);
                    int moveY = MOVE + inter.boundingRect().height();
                    QPoint leftTop = QPoint(x(), topPoint);
                    QPoint move = QPoint(0, moveY);
                    QSet<int> alreadyMove;
                    Move(leftTop, move, alreadyMove);
                    ap = a->GetPolygon();
                    bp = b->GetPolygon();
                }
            }
        }
    }
}

void TBlockVerticalLine::ClearBlockConnections() {
    QSet<TBlockDiagram*>::iterator it;
    for (it = connectedBlocks.begin(); it != connectedBlocks.end(); it++) {
        (*it)->RemoveBlockConnections(this);
    }
    connectedBlocks.clear();
}

void TBlockVerticalLine::RemoveBlockConnections(TBlockDiagram *other) {
    connectedBlocks.remove(other);
}

QVector<QPoint> *TBlockVerticalLine::GetConnectPoints() {
    connectPoints[0] = QPoint(x(), y());
    connectPoints[1] = QPoint(x(), y()+blockH);
    return &connectPoints;
}

void TBlockVerticalLine::Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) {
    if (alreadyMove.contains(id)) {
        return;
    }
    alreadyMove.insert(id);
    if (leftTop.y() > y() + blockH) {
        return;
    }
    if (leftTop.y() > y()) {
        blockH += move.y();
    } else {
        setY(y() + move.y());
    }
    if (leftTop.x() < x()) {
        setX(x() + move.x());
    }
    update();
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    for (; it != connectedBlocks.end(); it++) {
        if (!alreadyMove.contains((*it)->GetItemId())) {
            (*it)->Move(leftTop, move, alreadyMove);
        }
    }
}

TError TBlockVerticalLine::Verify(QVector<TBlockDiagram*> intersects) {
    if (connectedBlocks.size() < 2) {
        return VerifyVerticalErrorConnBlocksSum;
    }
    for (int i = 0; i < intersects.size(); i++) {
        if (!connectedBlocks.contains(intersects.at(i))) {
            return VerifyVerticalErrorUnexpectedIntersects;
        }
    }
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    int count = 0;
    for (; it != connectedBlocks.end(); it++) {
        UpdateVerifyCount(count, (*it)->GetType(), (*it)->GetConnectPoints());
    }
    if (count != 2) {
        return VerifyVerticalErrorConnBlocksStartEnd;
    }
    return Ok;
}

void TBlockVerticalLine::UpdateVerifyCount(int &count, const TBlockType type, QVector<QPoint> *otherPoints) {
    QVector<QPoint> *thisPoints = GetConnectPoints();

    if (type == TBeginEnd) {
        for (int i = 0; i < otherPoints->size(); i++) {
            QPoint distance0 = otherPoints->at(i) -  thisPoints->at(0);
            QPoint distance1 = otherPoints->at(i) -  thisPoints->at(1);
            if ((qAbs(distance0.x()) < POINTS_DELTA && qAbs(distance0.y()) < POINTS_DELTA) ||
                    (qAbs(distance1.x()) < POINTS_DELTA && qAbs(distance1.y()) < POINTS_DELTA))
            {
                count++;
            }
        }
        return;
    }

    if (type == THorizontalLine) {
        QPoint start = otherPoints->at(0);
        QPoint end = otherPoints->at(1);
        QPoint this1 = thisPoints->at(0);
        QPoint this2 = thisPoints->at(1);

        //topPoint
        if ((this1.x() > start.x() - POINTS_DELTA && this1.x() < end.x() + POINTS_DELTA &&
             this1.y() > (start.y() - POINTS_DELTA) && this1.y() < (end.y() + POINTS_DELTA)))
        {
            count++;
            return;
        }

        //botPoint
        if (this2.x() > start.x() - POINTS_DELTA  && this2.x() < end.x() + POINTS_DELTA  &&
                this2.y() > (start.y() - POINTS_DELTA) && this2.y() < (end.y() + POINTS_DELTA))
        {
            count++;
        }
    }
}

TBlockHorizontalLine::TBlockHorizontalLine(TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    id = 0;
    diagramId = 0;
    setPos(0, 0);
    blockW = 300;
    blockH = 0;
    connectPoints.resize(2);
    a = 0;
    b = 0;
    blockType = THorizontalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    setZValue(TBlockType::THorizontalLine);
}

TBlockHorizontalLine::TBlockHorizontalLine(int id, int dId, TBlockDiagram *parent)
    : TBlockDiagram(parent)
{
    this->id = id;
    diagramId = dId;
    setPos(0, 0);
    blockW = 300;
    blockH = 0;
    connectPoints.resize(2);
    a = 0;
    b = 0;
    blockType = THorizontalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    setZValue(TBlockType::THorizontalLine);
}

TBlockHorizontalLine::TBlockHorizontalLine(int id, int dId, int x, int y, int w) {
    blockType = THorizontalLine;
    setFlag(QGraphicsObject::ItemIsMovable);
    setFlag(QGraphicsObject::ItemIsSelectable);
    setFlag(QGraphicsObject::ItemIsFocusable);
    this->id = id;
    diagramId = dId;
    blockW = w;
    blockH = 0;
    a = 0;
    b = 0;
    setPos(x, y);
    connectPoints.resize(2);
    setZValue(TBlockType::THorizontalLine);
}

TBlockHorizontalLine::~TBlockHorizontalLine() {
}

void TBlockHorizontalLine::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *parent) {
    painter->setRenderHint(QPainter::HighQualityAntialiasing);
    QPen pen(Qt::black);
    pen.setWidth(PEN_WIDTH);
    painter->setPen(pen);
    painter->drawLine(0, 0, blockW, 0);
    if (isSelected()) {
        QBrush brush(Qt::green);
        pen.setWidth(PEN_WIDTH/2);
        painter->setPen(pen);
        painter->setBrush(brush);
        painter->drawRect(resizeRectX, resizeRectY, resizeRectWidth, resizeRectHeight);
        painter->drawRect(blockW + resizeRectX, resizeRectY, resizeRectWidth, resizeRectHeight);
    }
}

QRectF TBlockHorizontalLine::boundingRect() const {
    return QRectF(- PEN_WIDTH / 2 + resizeRectX,
                  - PEN_WIDTH / 2 + resizeRectY,
                  PEN_WIDTH + resizeRectWidth + blockW,
                  PEN_WIDTH + resizeRectHeight);
}

QPolygon TBlockHorizontalLine::GetPolygon() const
{
    QPolygon pol;
    int x = pos().x();
    int y = pos().y();
    pol << QPoint(x, y) << QPoint(x+blockW, y);
    pol << QPoint(x, y);
    return pol;
}

int TBlockHorizontalLine::GetX() {
    return x();
}

int TBlockHorizontalLine::GetY() {
    return y();
}

int TBlockHorizontalLine::GetWidth() {
    return blockW;
}

int TBlockHorizontalLine::GetHeight() {
    return blockH;
}

int TBlockHorizontalLine::GetA() {
    return a;
}

int TBlockHorizontalLine::GetB() {
    return b;
}

void TBlockHorizontalLine::UpdatePos(QPoint delta) {
    setPos(pos() + delta);
    update();
}

void TBlockHorizontalLine::AddBlock(TBlockDiagram *other) {
    connectedBlocks.insert(other);
    CheckForMove();
}

void TBlockHorizontalLine::CheckForMove() {
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    QSet<TBlockDiagram*>::iterator jt;
    for (; it != connectedBlocks.end(); it++) {
        TBlockDiagram* a = *it;
        for (jt = it + 1; jt != connectedBlocks.end(); jt++) {
            TBlockDiagram* b = *jt;
            if (a->GetType() != TBlockType::THorizontalLine && b->GetType() != TBlockType::THorizontalLine) {
                QPolygon ap = a->GetPolygon();
                QPolygon bp = b->GetPolygon();
                while (ap.intersects(bp)) {
                    if (ap.at(0).x() == bp.at(0).x()) {
                        b->UpdatePos(QPoint(2, 0));
                        bp = b->GetPolygon();
                    }
                    int leftPoint = ap.point(0).x() < bp.point(0).x() ? bp.point(0).x() : ap.point(0).x();
                    QPolygon inter = ap.intersected(bp);
                    int moveX = inter.boundingRect().width() + MOVE;
                    QPoint leftTop = QPoint(leftPoint, y());
                    QPoint move = QPoint(moveX, 0);
                    QSet<int> alreadyMove;
                    Move(leftTop, move, alreadyMove);
                    ap = a->GetPolygon();
                    bp = b->GetPolygon();
                }
            }
        }
    }
}

void TBlockHorizontalLine::ClearBlockConnections() {
    QSet<TBlockDiagram*>::iterator it;
    for (it = connectedBlocks.begin(); it != connectedBlocks.end(); it++) {
        (*it)->RemoveBlockConnections(this);
    }
    connectedBlocks.clear();
}

void TBlockHorizontalLine::RemoveBlockConnections(TBlockDiagram *other) {
    connectedBlocks.remove(other);
}

QVector<QPoint> *TBlockHorizontalLine::GetConnectPoints() {
    connectPoints[0] = QPoint(x(), y());
    connectPoints[1] = QPoint(x()+blockW, y());
    return &connectPoints;
}

void TBlockHorizontalLine::Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) {
    if (alreadyMove.contains(id)) {
        return;
    }
    alreadyMove.insert(id);
    if (leftTop.x() < x()) {
        blockW += move.x();
    } else {
        setX(x() + move.x());
    }
    if (leftTop.y() < y()) {
        setY(y() + move.y());
    }
    update();
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    for (; it != connectedBlocks.end(); it++) {
        if (!alreadyMove.contains((*it)->GetItemId())) {
            (*it)->Move(leftTop, move, alreadyMove);
        }
    }
}

TError TBlockHorizontalLine::Verify(QVector<TBlockDiagram*> intersects) {
    if (connectedBlocks.size() < 2) {
        return VerifyHorizontalErrorConnBlocksSum;
    }
    for (int i = 0; i < intersects.size(); i++) {
        if (!connectedBlocks.contains(intersects.at(i))) {
            return VerifyHorizontalErrorUnexpectedIntersects;
        }
    }
    QSet<TBlockDiagram*>::iterator it = connectedBlocks.begin();
    int count = 0;
    for (; it != connectedBlocks.end(); it++) {
        UpdateVerifyCount(count, (*it)->GetType(), (*it)->GetConnectPoints());
    }
    if (count != 2) {
        return VerifyHorizontalErrorConnBlocksStartEnd;
    }
    return Ok;
}

void TBlockHorizontalLine::UpdateVerifyCount(int &count, const TBlockType type, QVector<QPoint> *otherPoints) {
    QVector<QPoint> *thisPoints = GetConnectPoints();
    if (type == TVerticalLine) {
        QPoint start = otherPoints->at(0);
        QPoint end = otherPoints->at(1);
        QPoint this1 = thisPoints->at(0);
        QPoint this2 = thisPoints->at(1);

        //left point
        if (this1.x() > (start.x() - POINTS_DELTA) && this1.x() < (end.x()+POINTS_DELTA) &&
                this1.y() > start.y() - POINTS_DELTA && this1.y() < end.y() + POINTS_DELTA)
        {
            count++;
            return;
        }

        //right point
        if (this2.x() > (start.x() - POINTS_DELTA) && this2.x() < (end.x()+POINTS_DELTA) &&
                this2.y() > start.y() - POINTS_DELTA && this2.y() < end.y() + POINTS_DELTA)
        {
            count++;
        }
        return;
    }
    for (int i = 0; i < otherPoints->size(); i++) {
        QPoint distance0 = otherPoints->at(i) -  thisPoints->at(0);
        QPoint distance1 = otherPoints->at(i) -  thisPoints->at(1);
        if ((qAbs(distance0.x()) < POINTS_DELTA && qAbs(distance0.y()) < POINTS_DELTA) ||
                (qAbs(distance1.x()) < POINTS_DELTA && qAbs(distance1.y()) < POINTS_DELTA))
        {
            count++;
        }
    }
}

void TBlockHorizontalLine::mousePressEvent(QGraphicsSceneMouseEvent *e) {
    if (e->button() == Qt::LeftButton) {
        resizeFlagLeft = (e->pos().x() >= resizeRectX)
                && (e->pos().x() <= (resizeRectX + resizeRectWidth))
                && (e->pos().y() >= resizeRectY)
                && (e->pos().y() <= (resizeRectY + resizeRectHeight));
        resizeFlagRight = (e->pos().x() >= blockW + resizeRectX)
                && (e->pos().x() <= (blockW + resizeRectX + resizeRectWidth))
                && (e->pos().y() >= resizeRectY)
                && (e->pos().y() <= (resizeRectY + resizeRectHeight));
    }
    QGraphicsObject::mousePressEvent(e);
}

void TBlockHorizontalLine::mouseMoveEvent(QGraphicsSceneMouseEvent *e) {
    if (resizeFlagLeft) {
        prepareGeometryChange();
        int delta = e->pos().x();
        if (blockW - delta < MIN_HEIGHT) {
            delta = 0;
        }
        blockW -= delta;
        setX(x() + delta);
    } else if (resizeFlagRight) {
        prepareGeometryChange();
        blockW -= (blockW - e->pos().x());
        if (blockW < MIN_HEIGHT) {
            blockW = MIN_HEIGHT;
        }
    } else {
        QGraphicsObject::mouseMoveEvent(e);
    }
    update();
}

void TBlockHorizontalLine::mouseReleaseEvent(QGraphicsSceneMouseEvent *e) {
    resizeFlagLeft = false;
    resizeFlagRight = false;
    QGraphicsObject::mouseReleaseEvent(e);
    update();
    emit PosChanged();
}
