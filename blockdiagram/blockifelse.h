#pragma once
#include "textedit.h"
#include "blockdiagram.h"
#include <QGraphicsObject>

class TBlockIfElse: public TBlockDiagram
{
    Q_OBJECT
public:
    TBlockIfElse(TBlockDiagram *parent = nullptr);
    TBlockIfElse(int id, int dId, TBlockDiagram *parent = nullptr);
    TBlockIfElse(int id, int dId, QString text, int blockX, int blockY, int w, int h, int b);
    ~TBlockIfElse();
    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget *parent = nullptr) override;
    QRectF boundingRect() const override;
    QPolygon GetPolygon() const override;
    int GetX() override;
    int GetY() override;
    int GetWidth() override;
    int GetHeight() override;
    int GetA() override;
    int GetB() override;
    void UpdatePos(QPoint delta) override;
    void AddBlock(TBlockDiagram *other) override;
    void ClearBlockConnections() override;
    void RemoveBlockConnections(TBlockDiagram *other) override;
    QVector<QPoint>* GetConnectPoints() override;
    void Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) override;
    TError Verify(QVector<TBlockDiagram*> intersects) override;
private slots:
    void TextOk();
    void TextCancel();
    void SwapIfElse();
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
private:
    TBlockDiagram *verticalLine = nullptr;
    TBlockDiagram *horizontalLine = nullptr;
    QString right;
    QString down;
    TTextEdit *textEdit;
    const int resizeRectWidth = 30;
    const int resizeRectHeight = 30;
    int resizeRectX = -15;
    int resizeRectY = -15;
    bool resizeFlag = false;
};
