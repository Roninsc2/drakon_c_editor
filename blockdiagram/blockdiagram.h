#pragma once
#include <QRect>
#include <QString>
#include <QPolygon>
#include <QObject>
#include <QGraphicsObject>

#define PEN_WIDTH 5
#define WIDTH_TEXT_SIZE 16
#define HEIGHT_TEXT_SIZE 36
#define MIN_WIDTH 40
#define MIN_HEIGHT 70
#define FONT_SIZE 30
#define MOVE 50
#define POINTS_DELTA 40

typedef enum {
    TNull = 0,
    TVerticalLine = 1,
    THorizontalLine = 2,
    TAction = 3,
    TIfElse = 4,
    TBeginEnd = 5,
} TBlockType;

typedef enum {
    Ok = 0,
    VerifyVerticalErrorConnBlocksSum = 1,
    VerifyVerticalErrorConnBlocksStartEnd = 2,
    VerifyVerticalErrorUnexpectedIntersects = 3,

    VerifyHorizontalErrorConnBlocksSum = 4,
    VerifyHorizontalErrorConnBlocksStartEnd = 5,
    VerifyHorizontalErrorUnexpectedIntersects = 6,

    VerifyActionErrorConnToTwoLines = 7,
    VerifyActionErrorConnToZeroLines = 8,
    VerifyActionErrorIntersectsSize = 9,
    VerifyActionErrorIntersectsVertical = 10,
    VerifyActionErrorIntersectsHorizontal = 11,

    VerifyBeginEndErrorConnToVertical = 12,
    VerifyBeginEndErrorIntersectsSize = 13,
    VerifyBeginEndErrorIntersectsVertical = 14,
    VerifyBeginEndErrorIntersectsHorizontal = 15,
    VerifyBeginEndErrorNoStartEnd = 16,

    VerifyIfEsleErrorConnToLine = 17,
    VerifyIfEsleErrorIntersectsSize = 18,
    VerifyIfEsleErrorIntersectsLines = 19,
} TError;

class TBlockDiagram : public QGraphicsObject {

    Q_OBJECT
public:
    TBlockDiagram(QGraphicsObject *parent = nullptr);
    ~TBlockDiagram();
    TBlockType GetType();
    int GetItemId();
    int GetDiagramId();
    void SetItemId(int val);
    void SetDiagramId(int val);
    QString GetText();
    virtual void UpdatePos(QPoint delta) = 0;
    virtual void AddBlock(TBlockDiagram* other) = 0;
    virtual void ClearBlockConnections() = 0;
    virtual void RemoveBlockConnections(TBlockDiagram* other) = 0;
    virtual int GetX() = 0;
    virtual int GetY() = 0;
    virtual int GetWidth() = 0;
    virtual int GetHeight() = 0;
    virtual int GetA() = 0;
    virtual int GetB() = 0;
    virtual QPolygon GetPolygon() const  = 0;
    virtual QVector<QPoint>* GetConnectPoints()  = 0;
    virtual void Move(QPoint &leftTop, QPoint &move, QSet<int> &alreadyMove) = 0;
    virtual TError Verify(QVector<TBlockDiagram*> intersects) = 0;
signals:
    void PosChanged();
protected:
    TBlockType blockType = TNull;
    QVector<QPoint> connectPoints;
    int id;
    int diagramId;
    QString text;
    int blockW;
    int blockH;
    int a;
    int b;
};

