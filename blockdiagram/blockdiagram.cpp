#include "blockdiagram.h"

TBlockDiagram::TBlockDiagram(QGraphicsObject *parent)
    : QGraphicsObject(parent)
{
}

TBlockDiagram::~TBlockDiagram(){
}

TBlockType TBlockDiagram::GetType() {
    return blockType;
}

int TBlockDiagram::GetItemId() {
    return id;
}

int TBlockDiagram::GetDiagramId() {
    return diagramId;
}

void TBlockDiagram::SetItemId(int val) {
    id = val;
}

void TBlockDiagram::SetDiagramId(int val) {
    diagramId = val;
}

QString TBlockDiagram::GetText() {
    return text;
}
