#include "diagram.h"
#include "blockline.h"
#include "blockaction.h"
#include "blockbeginend.h"
#include "blockifelse.h"
#include <QtAlgorithms>
#include <QDebug>

static inline bool BlockLessThan(TBlockDiagram *first, TBlockDiagram *second) {
    return first->GetType() < second->GetType();
}

TDiagram::TDiagram() {
    SetupMapConnectionPoints();
}

TDiagram::TDiagram(int i, QString &n, QString d, double z) {
    id = i;
    name = n;
    descr = d;
    zoom = z;
    SetupMapConnectionPoints();
}

TDiagram::~TDiagram() {
    for (int i = 0; i < items.size(); i++) {
        if (items[i]) {
            delete items[i];
        }
    }
}

QString TDiagram::GetName() {
    return name;
}

int TDiagram::GetId() {
    return id;
}

QString TDiagram::GetDescription() {
    return descr;
}

double TDiagram::GetZoom() {
    return zoom;
}

void TDiagram::SetZoom(double z) {
    zoom = z;
}

void TDiagram::InsertBlock(TBlockDiagram *block) {
    connect(block, SIGNAL(PosChanged()), this, SLOT(UpdateBlockConnections()));
    items.push_back(block);
    CreateBlockConnection(block);
}

void TDiagram::InsertBlock(TBlockDiagram *block, int &maxItemId) {
    if (!block->GetItemId() && !block->GetDiagramId()) {
        block->SetItemId(++maxItemId);
        block->SetDiagramId(id);
    }
    connect(block, SIGNAL(PosChanged()), this, SLOT(UpdateBlockConnections()));
    items.push_back(block);
    if (maxItemId < block->GetItemId()) {
        maxItemId = block->GetItemId();
    }
    CreateBlockConnection(block);
}

void TDiagram::RemoveBlock(TBlockDiagram *block) {
    for (int i = 0; i < items.size(); i++) {
        if (items.at(i)->GetItemId() == block->GetItemId()) {
            items.remove(i);
        }
    }
}

void TDiagram::Clear() {
    for (int i = 0; i < items.size(); i++) {
        items.at(i)->ClearBlockConnections();
        items.at(i)->setParent(nullptr);
        delete items.at(i);
    }
}

QVector<TBlockDiagram *> *TDiagram::GetBlocks() {
    return &items;
}

TError TDiagram::Verify() {
    qSort(items.begin(), items.end(), BlockLessThan);
    for (int i = 0; i < items.size(); i++) {
        CreateBlockConnection(items.at(i));
    }
    int beginEndCount = 0;
    TError err = Ok;
    for (int i = 0; i < items.size(); i++) {
        if (items.at(i)->GetType() == TBeginEnd) {
            beginEndCount++;
        }
        QPolygon line = items.at(i)->GetPolygon();
        QVector<TBlockDiagram *> intersects;
        for (int j = 0; j < items.size(); j++) {
            if (items.at(i) != items.at(j) &&
                    line.intersects(items.at(j)->GetPolygon()))
            {
                intersects.push_back(items.at(j));
            }
        }
        err = items.at(i)->Verify(intersects);
        if (err) {
            return err;
        }
    }
    if (beginEndCount != 2) {
        err = VerifyBeginEndErrorNoStartEnd;
    }
    return err;
}

void TDiagram::UpdateBlockConnections() {
    TBlockDiagram* block = (TBlockDiagram*)(sender());
    CreateBlockConnection(block);
}

void TDiagram::CreateBlockConnection(TBlockDiagram *block) {
    block->ClearBlockConnections();
    for (int i = 0; i < items.size(); i++) {
        if (block != items[i]) {
            CreateBlocksConnections(block, items[i]);
        }
    }
}

void TDiagram::CreateBlocksConnections(TBlockDiagram* updated, TBlockDiagram* other) {
    QVector<QPoint> *upPoints = updated->GetConnectPoints();
    QVector<QPoint> *otherPoints = other->GetConnectPoints();
    QVector<int> upItPoints = connectionPoints[QPair<TBlockType, TBlockType>(updated->GetType(), other->GetType())];
    QVector<int> otherItPoints = connectionPoints[QPair<TBlockType, TBlockType>(other->GetType(), updated->GetType())];
    for (int i = 0; i < upItPoints.size(); i++) {
        int it = upItPoints.at(i);
        for (int j = 0; j < otherItPoints.size(); j++) {
            int jt = otherItPoints.at(j);
            QPoint distance = otherPoints->at(jt) -  upPoints->at(it);
            if (std::abs(distance.x()) < POINTS_DELTA && std::abs(distance.y()) < POINTS_DELTA) {
                updated->UpdatePos(distance);
                ConnectBlocks(updated, other);
                return;
            }
        }
    }
    QPoint start;
    QPoint end;
    if (other->GetType() == TBlockType::TVerticalLine) {
        start = otherPoints->at(0);
        end = otherPoints->at(1);
        for (int i = 0; i < upItPoints.size(); i++) {
            QPoint p = upPoints->at(upItPoints.at(i));
            if (p.x() > (start.x() - POINTS_DELTA) && p.x() < (end.x()+POINTS_DELTA) &&
                    p.y() > start.y() - POINTS_DELTA  && p.y() < end.y() + POINTS_DELTA )
            {
                updated->UpdatePos(QPoint(start.x() - p.x(), 0));
                ConnectBlocks(updated, other);
                return;
            }
        }
    }
    if (other->GetType() == TBlockType::THorizontalLine &&
            updated->GetType() == TBlockType::TVerticalLine)
    {
        start = otherPoints->at(0);
        end = otherPoints->at(1);
        for (int i = 0; i < upItPoints.size(); i++) {
            QPoint p = upPoints->at(upItPoints.at(i));
            if (p.x() > start.x() - POINTS_DELTA  && p.x() < end.x() + POINTS_DELTA  &&
                    p.y() > (start.y() - POINTS_DELTA) && p.y() < (end.y() + POINTS_DELTA))
            {
                updated->UpdatePos(QPoint(0, start.y() - p.y()));
                ConnectBlocks(updated, other);
                return;
            }

        }
    }
}

void TDiagram::ConnectBlocks(TBlockDiagram *first, TBlockDiagram *second) {
    first->AddBlock(second);
    second->AddBlock(first);
}

void TDiagram::SetupMapConnectionPoints() {
    QVector<int> zeroOnePoints;
    zeroOnePoints.push_back(0);
    zeroOnePoints.push_back(1);

    QVector<int> twoPoint;
    twoPoint.push_back(2);

    connectionPoints[QPair<TBlockType, TBlockType>(TAction, TAction)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TVerticalLine, TVerticalLine)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(THorizontalLine, THorizontalLine)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TBeginEnd, TBeginEnd)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TIfElse, TIfElse)] = QVector<int>();

    connectionPoints[QPair<TBlockType, TBlockType>(TAction, TBeginEnd)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TAction, TIfElse)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TAction, THorizontalLine)] = twoPoint;
    connectionPoints[QPair<TBlockType, TBlockType>(TAction, TVerticalLine)] = zeroOnePoints;

    connectionPoints[QPair<TBlockType, TBlockType>(TBeginEnd, TAction)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TBeginEnd, TIfElse)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TBeginEnd, THorizontalLine)] = twoPoint;
    connectionPoints[QPair<TBlockType, TBlockType>(TBeginEnd, TVerticalLine)] = zeroOnePoints;

    connectionPoints[QPair<TBlockType, TBlockType>(THorizontalLine, TIfElse)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(THorizontalLine, TBeginEnd)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(THorizontalLine, TAction)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(THorizontalLine, TVerticalLine)] = zeroOnePoints;

    connectionPoints[QPair<TBlockType, TBlockType>(TVerticalLine, TIfElse)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(TVerticalLine, TBeginEnd)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(TVerticalLine, THorizontalLine)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(TVerticalLine, TAction)] = zeroOnePoints;

    connectionPoints[QPair<TBlockType, TBlockType>(TIfElse, TVerticalLine)] = zeroOnePoints;
    connectionPoints[QPair<TBlockType, TBlockType>(TIfElse, THorizontalLine)] = twoPoint;
    connectionPoints[QPair<TBlockType, TBlockType>(TIfElse, TAction)] = QVector<int>();
    connectionPoints[QPair<TBlockType, TBlockType>(TIfElse, TBeginEnd)] = QVector<int>();
}
