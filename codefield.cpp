#include "codefield.h"
#include <QGraphicsRectItem>
#include <QPushButton>
#include <QGraphicsProxyWidget>
#include <QDebug>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include "blockdiagram/blockaction.h"
#include "blockdiagram/blockline.h"
#include "blockdiagram/blockbeginend.h"
#include "blockdiagram/blockifelse.h"

TCodeField::TCodeField(TDiagramsData *d, QWidget *parent)
    : QWidget(parent), data(d)
{
    scene = new QGraphicsScene(0, 0, width(), height(), this);
    scene->setBackgroundBrush(QBrush(Qt::darkGray));
    viewField = new QGraphicsView(scene);
    layout = new QVBoxLayout();
    layout->addWidget(viewField);
    setLayout(layout);
    SetupFiledMenu();
    connect(data, SIGNAL(DataRemoved()), scene, SLOT(clear()));
    connect(data, SIGNAL(CurrentDiagramChanged()), this, SLOT(PullDiagramData()));
}

TCodeField::~TCodeField() {
    delete moveUp;
    delete moveDown;
    delete moveLeft;
    delete moveRight;
}

void TCodeField::SetupFiledMenu() {
    fieldMenu = new TCodeFieldMenu(this);

    connect(fieldMenu, SIGNAL(AddAction()), this, SLOT(AddNewActionDiagram()));
    connect(fieldMenu, SIGNAL(AddBeginEnd()), this, SLOT(AddNewBeginEndDiagram()));
    connect(fieldMenu, SIGNAL(AddVertical()), this, SLOT(AddNewVerticalDiagram()));
    connect(fieldMenu, SIGNAL(AddHorizontal()), this, SLOT(AddNewHorizontalDiagram()));
    connect(fieldMenu, SIGNAL(AddIfElse()), this, SLOT(AddNewIfElseDiagram()));

    moveDown = new QAction(tr("Move Down"));
    moveDown->setShortcut(Qt::Key_Down);
    addAction(moveDown);
    connect(moveDown, SIGNAL(triggered()), this, SLOT(MoveDown()));
    moveUp = new QAction(tr("Move Up"));
    moveUp->setShortcut(Qt::Key_Up);
    addAction(moveUp);
    connect(moveUp, SIGNAL(triggered()), this, SLOT(MoveUp()));
    moveLeft = new QAction(tr("Move Left"));
    moveLeft->setShortcut(Qt::Key_Left);
    addAction(moveLeft);
    connect(moveLeft, SIGNAL(triggered()), this, SLOT(MoveLeft()));
    moveRight = new QAction(tr("Move Right"));
    moveRight->setShortcut(Qt::Key_Right);
    addAction(moveRight);
    connect(moveRight, SIGNAL(triggered()), this, SLOT(MoveRight()));
}

void TCodeField::mousePressEvent(QMouseEvent *e) {
    if(e->button()==Qt::RightButton) {
        fieldMenu->exec(QCursor::pos());
    }
    QWidget::mousePressEvent(e);
}

void TCodeField::wheelEvent(QWheelEvent *e) {
    double zoom = 1.0;
    if (e->delta() > 0) {
        zoom = 0.99;
    } else {
        zoom = 1.01;
    }
    viewField->scale(zoom, zoom);
    data->SetZoom(viewField->transform().m11());
    QWidget::wheelEvent(e);

}

void TCodeField::MoveDown() {
    QList<QGraphicsItem *> items = scene->items();
    for (int i = 0; i < items.size(); i++) {
        items.at(i)->moveBy(0, MOVE_FIELD);
    }
}

void TCodeField::MoveUp() {
    QList<QGraphicsItem *> items = scene->items();
    for (int i = 0; i < items.size(); i++) {
        items.at(i)->moveBy(0, -MOVE_FIELD);
    }
}

void TCodeField::MoveLeft() {
    QList<QGraphicsItem *> items = scene->items();
    for (int i = 0; i < items.size(); i++) {
        items.at(i)->moveBy(-MOVE_FIELD, 0);
    }
}

void TCodeField::MoveRight() {
    QList<QGraphicsItem *> items = scene->items();
    for (int i = 0; i < items.size(); i++) {
        items.at(i)->moveBy(MOVE_FIELD, 0);
    }
}

void TCodeField::PullDiagramData() {
    while (scene->items().size() > 0) {
        scene->removeItem(scene->items().at(0));
    }
    viewField->resetMatrix();
    viewField->scale(data->GetZoom(), data->GetZoom());
    QVector<TBlockDiagram *> *currentBlocks = data->GetCurrentBlocks();
    for (int i = 0; i < currentBlocks->size(); i++) {
        switch ((*currentBlocks)[i]->GetType()) {
        case TAction: {
            scene->addItem(static_cast<TBlockAction*>((*currentBlocks)[i]));
            break;
        }
        case TBeginEnd: {
            scene->addItem(static_cast<TBlockBeginEnd*>((*currentBlocks)[i]));
            break;
        }
        case TVerticalLine: {
            scene->addItem(static_cast<TBlockVerticalLine*>((*currentBlocks)[i]));
            break;
        }
        case THorizontalLine: {
            scene->addItem(static_cast<TBlockHorizontalLine*>((*currentBlocks)[i]));
            break;
        }
        case TIfElse: {
            scene->addItem(static_cast<TBlockIfElse*>((*currentBlocks)[i]));
            break;
        }
        default:
            break;
        }
    }
}

void TCodeField::AddNewActionDiagram() {
    if (data->IsValid()) {
        TBlockAction *block = new TBlockAction();
        data->AddNewBlock(block);
        scene->addItem(block);
    }
}

void TCodeField::AddNewBeginEndDiagram() {
    if (data->IsValid()) {
        TBlockBeginEnd *block = new TBlockBeginEnd();
        data->AddNewBlock(block);
        scene->addItem(block);
    }
}

void TCodeField::AddNewVerticalDiagram() {
    if (data->IsValid()) {
        TBlockVerticalLine *line = new TBlockVerticalLine();
        data->AddNewBlock(line);
        scene->addItem(line);
    }
}

void TCodeField::AddNewHorizontalDiagram() {
    if (data->IsValid()) {
        TBlockHorizontalLine *line = new TBlockHorizontalLine();
        data->AddNewBlock(line);
        scene->addItem(line);
    }
}

void TCodeField::AddNewIfElseDiagram() {
    if (data->IsValid()) {
        TBlockIfElse *ifElse = new TBlockIfElse();
        data->AddNewBlock(ifElse);
        scene->addItem(ifElse);
    }
}

void TCodeField::DoUndo() {
}

void TCodeField::DoCut() {
}

void TCodeField::DoCopy() {
}

void TCodeField::DoPaste() {
}

void TCodeField::DoDelete() {
    QGraphicsItem *item = scene->focusItem();
    if (item) {
        data->DeleteBlock(static_cast<TBlockDiagram*>(item));
        scene->removeItem(item);
    }
}
