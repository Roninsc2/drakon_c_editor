#include "insertmenu.h"
#include "QVBoxLayout"

TCodeFieldMenu::TCodeFieldMenu(QWidget *parent) : QMenu(parent) {
    SetupMenu();
}

TCodeFieldMenu::~TCodeFieldMenu() {
}

void TCodeFieldMenu::SetupMenu() {
    insert = addMenu(tr("Insert diagram"));

    insertAction = insert->addAction("Action");
    insertBeginEnd = insert->addAction("Begin/End");
    insertVertLine = insert->addAction("Vertical line");
    insertHorLine = insert->addAction("Horizontal line");
    insertIfElse = insert->addAction("If-Else");

    connect(insertAction, SIGNAL(triggered()), this, SIGNAL(AddAction()));
    connect(insertBeginEnd, SIGNAL(triggered()), this, SIGNAL(AddBeginEnd()));
    connect(insertVertLine, SIGNAL(triggered()), this, SIGNAL(AddVertical()));
    connect(insertHorLine, SIGNAL(triggered()), this, SIGNAL(AddHorizontal()));
    connect(insertIfElse, SIGNAL(triggered()), this, SIGNAL(AddIfElse()));
}
