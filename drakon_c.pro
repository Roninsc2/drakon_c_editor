QT       += core gui sql

CONFIG += dockwidget

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = drakon_c
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        blockdiagram/blockaction.cpp \
        blockdiagram/blockbeginend.cpp \
        blockdiagram/blockdiagram.cpp \
        blockdiagram/blockifelse.cpp \
        blockdiagram/blockline.cpp \
        blockdiagram/diagram.cpp \
        codefield.cpp \
        data/diagramsdata.cpp \
        data/filemanager.cpp \
        diagramstree.cpp \
        editor.cpp \
        insertmenu.cpp \
        main.cpp \
        textedit.cpp

HEADERS += \
    blockdiagram/blockaction.h \
    blockdiagram/blockbeginend.h \
    blockdiagram/blockdiagram.h \
    blockdiagram/blockifelse.h \
    blockdiagram/blockline.h \
    blockdiagram/diagram.h \
    codefield.h \
    data/diagramsdata.h \
    data/filemanager.h \
    diagramstree.h \
    editor.h \
    insertmenu.h \
    textedit.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
