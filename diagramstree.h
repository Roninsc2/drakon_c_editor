#pragma once
#include <QTreeWidget>
#include "data/diagramsdata.h"

class TDiagramsTree : public QTreeWidget
{
    Q_OBJECT
public:
    explicit TDiagramsTree(TDiagramsData *data, QWidget *parent = nullptr);
    ~TDiagramsTree();
public slots:
    void DoUndo();
    void SetupTree();
    void OnItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);
    void DoDelete();
private:
    void AddNextLevelItem(QTreeWidgetItem *item, QString name);
private:
    TDiagramsData *data;
};
