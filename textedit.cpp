#include "textedit.h"

TTextEdit::TTextEdit(bool ifEsleFlag, QWidget *parent) : QWidget(parent)
{
    text = new QTextEdit(this);
    ok = new QPushButton(tr("Ok"),this);
    cancel = new QPushButton(tr("Cancel"), this);
    connect(ok, SIGNAL(clicked()), this, SIGNAL(TextOk()));
    connect(cancel, SIGNAL(clicked()), this, SIGNAL(TextCancel()));

    layout = new QGridLayout(this);
    layout->addWidget(text, 1, 1, 4, 6);
    layout->addWidget(ok, 5, 1, 1, 1);
    layout->addWidget(cancel, 5, 3, 1, 1);
    if (ifEsleFlag) {
        swapIfElse = new QPushButton(tr("Swap If-Else"), this);
        connect(swapIfElse, SIGNAL(clicked()), this, SIGNAL(SwapIfElse()));
        layout->addWidget(swapIfElse, 5, 2, 1, 1);
    }
    setLayout(layout);
}

TTextEdit::~TTextEdit() {
    delete text;
    delete ok;
    delete cancel;
    delete layout;
    if (swapIfElse) {
        delete swapIfElse;
    }
}

void TTextEdit::GetText(QString &t) {
    t = text->toPlainText();
}

void TTextEdit::SetText(QString t) {
    text->setText(t);
}
