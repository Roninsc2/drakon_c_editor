#pragma once

#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QTextEdit>

class TTextEdit : public QWidget
{
    Q_OBJECT
public:
    explicit TTextEdit(bool ifElseFlag = false, QWidget *parent = nullptr);
    ~TTextEdit();
    void GetText(QString &text);
    void SetText(QString text);
signals:
    void TextOk();
    void TextCancel();
    void SwapIfElse();
private:
    QTextEdit *text;
    QGridLayout *layout;
    QPushButton * ok;
    QPushButton *cancel;
    QPushButton *swapIfElse = nullptr;
};

