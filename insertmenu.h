#pragma once

#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QWidget>
#include <QToolBar>

class TCodeFieldMenu : public QMenu
{
    Q_OBJECT
public:
    explicit TCodeFieldMenu(QWidget *parent = nullptr);
    ~TCodeFieldMenu();

signals:
    void AddAction();
    void AddBeginEnd();
    void AddVertical();
    void AddHorizontal();
    void AddIfElse();
private:
    void SetupMenu();
private:
    QMenu *insert;
    QAction *insertAction;
    QAction *insertBeginEnd;
    QAction *insertVertLine;
    QAction *insertHorLine;
    QAction *insertIfElse;
};

