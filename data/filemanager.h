#pragma once
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QMap>

class TDiagram;

class TSqlFile
{
public:
    TSqlFile();
    ~TSqlFile();
    void Open(QString &file, QMap<QString, TDiagram*> &diagrams, QMap<QString, QVector<QString>> &tree, int &maxItemId);
    void Save(QString &file, QMap<QString, TDiagram*> &diagrams, QMap<QString, QVector<QString>> &tree);
    void NewFile(QString &file, QMap<QString, TDiagram*> &diagrams, QMap<QString, QVector<QString>> &tree, int &maxItemId);
private:
    void ParseTreeNodes(QMap<QString, QVector<QString>> &tree, QMap<int, TDiagram *> &idMap);
    void ParseDiagramInfo();
    void ParseInfo();
    void ParseDiagrams(QMap<QString, TDiagram*> &diagrams);
    void ParseItems(QMap<int, TDiagram *> &idMap, int &maxItemId);
    void ParseState();

    void SaveTreeNodes(QMap<QString, QVector<QString>> &tree, QMap<QString, TDiagram*> &diagrams);
    void SaveNextLvlTreeNodes(QMap<QString, QVector<QString>> &tree, QMap<QString, TDiagram*> &diagrams,
                              QString parentName, int lvl, int &nodeId);
    void SaveDiagramInfo();
    void SaveInfo();
    void SaveDiagrams(QMap<QString, TDiagram*> &diagrams);
    void SaveItems(QMap<QString, TDiagram*> &diagrams);
    void SaveState();

    void CreateTreeNodes();
    void CreateDiagramInfo();
    void CreateInfo();
    void CreateDiagrams(QMap<QString, TDiagram*> &diagrams);
    void CreateItems(QMap<QString, TDiagram*> &diagrams, int &maxItemId);
    void CreateState();

    QMap<int, TDiagram*> GetIdDgrmMap(QMap<QString, TDiagram*> &diagrams);
private:
    QMap<QString, int> typeWordToInt;
    QMap<int, QString> typeIntToWord;
    QSqlDatabase db;
    QSqlQuery query;
};
