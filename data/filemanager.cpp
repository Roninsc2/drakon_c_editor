#include "filemanager.h"
#include <QDebug>
#include <QString>
#include <QSqlQuery>
#include <QSqlError>
#include <QFileDialog>
#include "blockdiagram/blockdiagram.h"
#include "blockdiagram/diagram.h"
#include "blockdiagram/blockaction.h"
#include "blockdiagram/blockbeginend.h"
#include "blockdiagram/blockline.h"
#include "blockdiagram/blockifelse.h"


TSqlFile::TSqlFile() {
    db = QSqlDatabase::addDatabase("QSQLITE");
    query = QSqlQuery(db);
    typeWordToInt[""] = 0;
    typeWordToInt["vertical"] = 1;
    typeWordToInt["horizontal"] = 2;
    typeWordToInt["action"] = 3;
    typeWordToInt["if"] = 4;
    typeWordToInt["beginend"] = 5;

    typeIntToWord[0] = "";
    typeIntToWord[1] = "vertical";
    typeIntToWord[2] = "horizontal";
    typeIntToWord[3] = "action";
    typeIntToWord[4] = "if";
    typeIntToWord[5] = "beginend";
}

TSqlFile::~TSqlFile() {
    query.clear();
    db.close();
}

void TSqlFile::Open(QString &fileName,
                     QMap<QString, TDiagram*> &diagrams,
                     QMap<QString, QVector<QString>> &tree,
                     int &maxItemId)
{
    db.setDatabaseName(fileName);
    db.open();
    ParseDiagrams(diagrams);
    QMap<int, TDiagram *> idMap = GetIdDgrmMap(diagrams);
    ParseItems(idMap, maxItemId);
    ParseTreeNodes(tree, idMap);
    db.close();
    query.clear();
}

void TSqlFile::Save(QString &fileName,
                    QMap<QString, TDiagram*> &diagrams,
                    QMap<QString, QVector<QString>> &tree)
{
    db.setDatabaseName(fileName);
    db.open();
    if (!query.exec("DELETE FROM tree_nodes")) {
        qDebug("Error occurred querying.");
    }
    if (!query.exec("DELETE FROM diagrams")) {
        qDebug("Error occurred querying.");
    }
    if (!query.exec("DELETE FROM items")) {
        qDebug("Error occurred querying.");
    }
    if (!query.exec("DELETE FROM info")) {
        qDebug("Error occurred querying.");
    }
    if (!query.exec("DELETE FROM diagram_info")) {
        qDebug("Error occurred querying.");
    }
    if (!query.exec("DELETE FROM state")) {
        qDebug("Error occurred querying.");
    }
    SaveTreeNodes(tree, diagrams);
    SaveDiagrams(diagrams);
    SaveItems(diagrams);
    SaveInfo();
    SaveState();
    SaveDiagramInfo();
    db.close();
    query.clear();
}

void TSqlFile::NewFile(QString &fileName, QMap<QString, TDiagram*> &diagrams,
                       QMap<QString, QVector<QString> > &tree, int &maxItemId)
{
    db.setDatabaseName(fileName);
    db.open();
    CreateTreeNodes();
    CreateDiagrams(diagrams);
    CreateItems(diagrams, maxItemId);
    QMap<int, TDiagram *> idMap = GetIdDgrmMap(diagrams);
    ParseTreeNodes(tree, idMap);
    CreateInfo();
    CreateState();
    CreateDiagramInfo();
    db.close();
    query.clear();
}

void TSqlFile::ParseTreeNodes(QMap<QString, QVector<QString>> &tree, QMap<int, TDiagram *> &idMap) {
    query.prepare("SELECT * FROM tree_nodes");
    if (!query.exec()) {
        qDebug("Error occurred querying.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return;
    }
    QMap<int, QString> folders;
    while (query.next()) {
        int parent = query.value(1).toInt();
        int nodeId = query.value(0).toInt();
        int dId = query.value(4).toInt();
        QString type = query.value(2).toString();
        QString name = query.value(3).toString();
        QString key;
        if (!parent) {
            key = "";

        } else {
            key = folders[parent];
        }
        if (type == "folder") {
            folders[nodeId] = name;
        }
        if (type == "item") {
            name = idMap[dId]->GetName();
        }
        tree[key].push_back(name);
    }
}

void TSqlFile::ParseDiagrams(QMap<QString, TDiagram*> &diagrams) {
    query.prepare("SELECT * FROM diagrams");
    if (!query.exec()) {
        qDebug("Error occurred querying.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return;
    }
    while (query.next()) {
        int id = query.value(0).toInt();
        QString name = query.value(1).toString();
        QString description = query.value(3).toString();
        double zoom = query.value(4).toDouble();
        diagrams.insert(name, new TDiagram(id, name, description, zoom/100.0));
    }
}
void TSqlFile::ParseItems(QMap<int, TDiagram *> &idMap, int &maxItemId) {
    query.prepare("SELECT * FROM items");
    if (!query.exec()) {
        qDebug("Error occurred querying.");
        qDebug("%s.", qPrintable(db.lastError().text()));
        return;
    }
    maxItemId = 0;
    while (query.next()) {
        int id = query.value(0).toInt();
        if (id > maxItemId) {
            maxItemId = id;
        }
        int dId = query.value(1).toInt();
        QString type = query.value(2).toString();
        QString text = query.value(3).toString();
        int x = query.value(5).toInt();
        int y = query.value(6).toInt();
        int w = query.value(7).toInt();
        int h = query.value(8).toInt();
        int a = query.value(9).toInt();
        int b = query.value(10).toInt();
        switch (typeWordToInt[type]) {
        case TVerticalLine: {
            TBlockVerticalLine *block = new TBlockVerticalLine(id, dId, x, y, h);
            idMap[dId]->InsertBlock(block);
            break;
        }
        case THorizontalLine: {
            TBlockHorizontalLine *block = new TBlockHorizontalLine(id, dId, x, y, w);
            idMap[dId]->InsertBlock(block);
            break;
        }
        case TAction: {
            TBlockAction *block = new TBlockAction(id, dId, text, x, y, w, h);
            idMap[dId]->InsertBlock(block);
            break;
        }
        case TBeginEnd: {
            TBlockBeginEnd *block = new TBlockBeginEnd(id, dId, text, x, y, w, h);
            idMap[dId]->InsertBlock(block);
            break;
        }
        case TIfElse: {
            TBlockIfElse *block = new TBlockIfElse(id, dId, text, x, y, w, h, b);
            idMap[dId]->InsertBlock(block);
            break;
        }
        default:
            break;
        }
    }
}

void TSqlFile::SaveTreeNodes(QMap<QString, QVector<QString>> &tree,
                             QMap<QString, TDiagram*> &diagrams)
{
    QVector<QString> lvl0 = tree[""];
    int lvl = 0;
    int nodeId = 0;
    for (int i = 0; i < lvl0.size(); i++) {
        nodeId++;
        QString type;
        QString name = lvl0[i];
        QVariant dId(QVariant::Int);
        if (!diagrams.contains(lvl0[i])) {
            type = "folder";
        } else {
            type = "item";
            dId = QVariant::fromValue(diagrams[name]->GetId());
            name.clear();
        }
        query.prepare("INSERT INTO tree_nodes (node_id, parent, type, name, diagram_id)"
                      "VALUES (?, ?, ?, ?, ?)");
        query.addBindValue(nodeId);
        query.addBindValue(lvl);
        query.addBindValue(type);
        query.addBindValue(name);
        query.addBindValue(dId);
        query.exec();
        if (type == "folder") {
            SaveNextLvlTreeNodes(tree, diagrams, name, nodeId, nodeId);
        }
    }
}

void TSqlFile::SaveNextLvlTreeNodes(QMap<QString, QVector<QString> > &tree,
                                    QMap<QString, TDiagram*> &diagrams,
                                    QString parentName, int lvl, int &nodeId)
{
    QVector<QString> nextLvl = tree[parentName];
    for (int i = 0; i < nextLvl.size(); i++) {
        nodeId++;
        QString type;
        QString name = nextLvl[i];
        QVariant dId(QVariant::Int);
        if (!diagrams.contains(nextLvl[i])) {
            type = "folder";
        } else {
            type = "item";
            dId = QVariant::fromValue(diagrams[name]->GetId());
            name.clear();
        }
        query.prepare("INSERT INTO tree_nodes (node_id, parent, type, name, diagram_id)"
                      "VALUES (?, ?, ?, ?, ?)");
        query.addBindValue(nodeId);
        query.addBindValue(lvl);
        query.addBindValue(type);
        query.addBindValue(name);
        query.addBindValue(dId);
        query.exec();
        if (type == "folder") {
            SaveNextLvlTreeNodes(tree, diagrams, name, nodeId, nodeId);
        }
    }
}
void TSqlFile::SaveDiagramInfo() {
    query.prepare("INSERT INTO diagram_info (diagram_id, name, value)"
                  "VALUES (?, ?, ?)");
    query.exec();
}
void TSqlFile::SaveInfo() {
    query.prepare("INSERT INTO info (key, value)"
                  "VALUES (?, ?)");

    QVariantList keys;
    keys << "type" << "version" << "start_version" << "language";
    query.addBindValue(keys);

    QVariantList vals;
    vals << "drakon" << "31" << "1" << "C";
    query.addBindValue(vals);

    query.execBatch();
}
void TSqlFile::SaveDiagrams(QMap<QString, TDiagram*> &diagrams) {
    QString origin = "0 0";
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        query.prepare("INSERT INTO diagrams (diagram_id, name, origin, description, zoom)"
                      "VALUES (?, ?, ?, ?, ?)");
        query.addBindValue(it.value()->GetId());
        query.addBindValue(it.value()->GetName());
        query.addBindValue(origin);
        query.addBindValue(it.value()->GetDescription());
        query.addBindValue(it.value()->GetZoom()*100.0);
        query.exec();
    }
}
void TSqlFile::SaveItems(QMap<QString, TDiagram*> &diagrams) {
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        QVector<TBlockDiagram *> *blocks = it.value()->GetBlocks();
        for (int i = 0; i < blocks->size(); i++) {
            TBlockDiagram * block = blocks->at(i);
            query.prepare("INSERT INTO items (item_id, diagram_id, type, text, selected, "
                          "x, y, w, h, a, b, aux_value, color, format, text2)"
                          "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            query.addBindValue(block->GetItemId());
            query.addBindValue(block->GetDiagramId());
            query.addBindValue(typeIntToWord[block->GetType()]);
            query.addBindValue(block->GetText());
            query.addBindValue(0);
            query.addBindValue(block->GetX());
            query.addBindValue(block->GetY());
            query.addBindValue(block->GetWidth());
            query.addBindValue(block->GetHeight());
            query.addBindValue(block->GetA());
            query.addBindValue(block->GetB());
            query.addBindValue(QVariant(QVariant::String));
            query.addBindValue(QVariant(QVariant::String));
            query.addBindValue(QVariant(QVariant::String));
            query.addBindValue(QVariant(QVariant::String));
            query.exec();
        }
    }
}

void TSqlFile::SaveState() {
    query.prepare("INSERT INTO state (row, current_dia, description)"
                  "VALUES (?, ?, ?)");
    query.addBindValue(QVariant(QVariant::Int));
    query.addBindValue(QVariant(QVariant::Int));
    query.addBindValue(QVariant(QVariant::String));
    query.exec();
}

void TSqlFile::CreateTreeNodes() {
    query.prepare("CREATE TABLE tree_nodes ( node_id integer primary key,"
                  " parent integer, type text, name text,"
                  " diagram_id integer )");
    query.exec();
    query.prepare("CREATE UNIQUE INDEX node_for_diagram on tree_nodes (diagram_id)");
    query.exec();
    query.prepare("INSERT INTO tree_nodes (node_id, parent, type, name, diagram_id)"
                  "VALUES (?, ?, ?, ?, ?)");
    query.addBindValue(1);
    query.addBindValue(0);
    query.addBindValue(QString("item"));
    query.addBindValue(QVariant(QVariant::String));
    query.addBindValue(1);
    query.exec();
}

void TSqlFile::CreateDiagramInfo()
{
    query.prepare("CREATE TABLE diagram_info (diagram_id integer, name text, "
                  "value text, primary key (diagram_id, name))");
    query.exec();
}

void TSqlFile::CreateInfo() {
    query.prepare("CREATE TABLE info( key text primary key,"
                  " value text )");
    query.exec();
    SaveInfo();
}

void TSqlFile::CreateDiagrams(QMap<QString, TDiagram*> &diagrams) {
    query.prepare("CREATE TABLE diagrams ( diagram_id integer primary key,"
                  " name text unique, origin text, description text,"
                  " zoom double )");
    query.exec();
    QString origin = "0 0";
    double zoom = 100.0;
    int id = 1;
    QString name = "Untitled";
    QString desrc;
    diagrams.insert(name, new TDiagram(id, name, desrc));
    query.prepare("INSERT INTO diagrams (diagram_id, name, origin, description, zoom)"
                  "VALUES (?, ?, ?, ?, ?)");
    query.addBindValue(id);
    query.addBindValue(name);
    query.addBindValue(origin);
    query.addBindValue(QVariant(QVariant::String));
    query.addBindValue(zoom);
    query.exec();
    ParseDiagrams(diagrams);
}

void TSqlFile::CreateItems(QMap<QString, TDiagram*> &diagrams, int &maxItemId) {
    query.prepare("CREATE TABLE items ( item_id integer primary key,"
                  " diagram_id integer, type text, text text, selected integer,"
                  " x integer, y integer, w integer, h integer, a integer,"
                  " b integer, aux_value integer, color text, format text,"
                  " text2 text )");
    query.exec();
    query.prepare("CREATE INDEX items_per_diagram on items (diagram_id)");
    query.exec();
    QString beginStr = "Untitled";
    QString endStr = "End";
    QString actStr;
    int id = 1;
    int dId = 1;
    int blockY = 0;
    int blockX = 0;
    int blockW = 100;
    int blockH = 75;
    int vertLineLen = 300;
    int horisonLineLen = 200;
    int lineOverlayLen = 25;
    TBlockBeginEnd *begin = new TBlockBeginEnd(id++, dId, beginStr, blockX, blockY, blockW, blockH);
    TBlockBeginEnd *end = new TBlockBeginEnd(id++, dId, endStr, blockX, blockY+vertLineLen+blockH, blockW, blockH);
    TBlockVerticalLine *vertical = new TBlockVerticalLine(id++, dId, blockX, blockY+blockH-lineOverlayLen, vertLineLen);
    TBlockHorizontalLine *horizontal = new TBlockHorizontalLine(id++, dId, blockX+blockW-lineOverlayLen, blockY, horisonLineLen);
    TBlockAction *action = new TBlockAction(id, dId, actStr, blockX+blockW*2+horisonLineLen-lineOverlayLen*2, blockY, blockW, blockH);
    maxItemId = id;
    diagrams[beginStr]->InsertBlock(begin);
    diagrams[beginStr]->InsertBlock(end);
    diagrams[beginStr]->InsertBlock(vertical);
    diagrams[beginStr]->InsertBlock(horizontal);
    diagrams[beginStr]->InsertBlock(action);
    SaveItems(diagrams);
}

void TSqlFile::CreateState() {
    query.prepare("CREATE TABLE state ( row integer primary key,"
                  " current_dia integer, description text )");
    query.exec();
    SaveState();
}

QMap<int, TDiagram *> TSqlFile::GetIdDgrmMap(QMap<QString, TDiagram*> &diagrams)
{
    QMap<int, TDiagram *> idMap;
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        idMap[it.value()->GetId()] = it.value();
    }
    return idMap;

}
