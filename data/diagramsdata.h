#pragma once

#include <QObject>
#include "blockdiagram/diagram.h"
#include "filemanager.h"
#include <QMap>

class TDiagramsData : public QObject {

    Q_OBJECT
public:
    TDiagramsData(QObject *parent = nullptr);
    ~TDiagramsData();
    QVector<TBlockDiagram*> *GetCurrentBlocks();
    QVector<QString> GetTreeItems(QString parent = "");
    bool GetTreeItemType(QString itemName); // false == item, true == folder
    bool IsValid();
    void AddNewBlock(TBlockDiagram *);
    void DeleteBlock(TBlockDiagram * block);
    void DeleteSelected(QString &name);
    void SetZoom(double z);
    double GetZoom();
    void AddNewDiagram(QString n, QString parent = "");
    void AddNewFolder(QString n, QString parent = "");
signals:
    void DataChanged();
    void DataRemoved();
    void CurrentDiagramChanged();
public slots:
    void VerifyCurrent();
    void VerifyAll();
    void NewFile();
    void SaveFile();
    void OpenFile();
    void SetCurrentDiagram(QString name);
private:
    void ClearData();
    void DeleteDiagram(QString name);
    void DeleteFolder(QString name);
    QString ErrorToString(TError err, TDiagram* d);
    void SetupErrorMap();
private:
    QMap<QString, TDiagram*> diagrams;
    QMap<TError, QString> errorsString;
    QMap<QString, QVector<QString>> tree; //parent -> diagrams, "" ~ topLevel
    TSqlFile file;
    QString fileName;
    TDiagram *currentDiagram = nullptr;
    int maxItemId = 0;
};
