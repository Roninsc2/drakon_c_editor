#include "diagramsdata.h"
#include <QFileDialog>
#include <QDebug>
#include <blockdiagram/blockaction.h>
#include <QMessageBox>

TDiagramsData::TDiagramsData(QObject *parent) : QObject(parent) {
    SetupErrorMap();
}

TDiagramsData::~TDiagramsData() {
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        delete it.value();
    }
}

QVector<TBlockDiagram *> *TDiagramsData::GetCurrentBlocks() {
    return currentDiagram->GetBlocks();
}

QVector<QString> TDiagramsData::GetTreeItems(QString parent) {
    return tree[parent];
}

bool TDiagramsData::GetTreeItemType(QString itemName) {
    return !diagrams.contains(itemName);
}

bool TDiagramsData::IsValid() {
    return (currentDiagram != nullptr);
}

void TDiagramsData::AddNewBlock(TBlockDiagram *block) {
    currentDiagram->InsertBlock(block, maxItemId);
}

void TDiagramsData::DeleteBlock(TBlockDiagram *block) {
    currentDiagram->RemoveBlock(block);
}


void TDiagramsData::SetZoom(double z) {
    currentDiagram->SetZoom(z);
}

double TDiagramsData::GetZoom() {
    return currentDiagram->GetZoom();
}

void TDiagramsData::DeleteSelected(QString &name) {
    if (tree.contains(name)) {
        DeleteFolder(name);
        QMap<QString, QVector<QString>>::iterator it;
        for (it = tree.begin(); it != tree.end(); it++) {
            for (int i = 0; i < it.value().size(); i++) {
                if (it.value().at(i) == name) {
                    it.value().remove(i);
                    break;
                }
            }
        }
    } else {
        DeleteDiagram(name);
    }
    emit DataChanged();
}

void TDiagramsData::DeleteDiagram(QString name) {
    QMap<QString, QVector<QString>>::iterator it;
    for (it = tree.begin(); it != tree.end(); it++) {
        for (int i = 0; i < it.value().size(); i++) {
            if (it.value().at(i) == name) {
                it.value().remove(i);
                if (currentDiagram == diagrams.value(name)) {
                    currentDiagram->Clear();
                    currentDiagram = nullptr;
                }
                diagrams.remove(name);
                return;
            }
        }
    }
}

void TDiagramsData::DeleteFolder(QString name) {
    QVector<QString> child = tree.value(name);
    for (int i = 0; i < child.size(); i++) {
        if (tree.contains(child.at(i))) {
            DeleteFolder(child.at(i));
        } else {
            DeleteDiagram(child.at(i));
        }
    }
    tree.remove(name);
}

QString TDiagramsData::ErrorToString(TError err, TDiagram *d) {
    return "Error at diagram " + d->GetName() + ".\n" + errorsString[err];
}

void TDiagramsData::SetupErrorMap() {
    errorsString[TError(0)] = "Verify Ok.";
    errorsString[TError(1)] = "Vertical line error: the number of connections is less than expected.";
    errorsString[TError(2)] = "Vertical line error: the ends of the line are not connected to other elements.";
    errorsString[TError(3)] = "Vertical line error: element has erroneous intersection.";
    errorsString[TError(4)] = "Horizontal line error: the number of connections is less than expected.";
    errorsString[TError(5)] = "Horizontal line error: the ends of the line are not connected to other elements.";
    errorsString[TError(6)] = "Horizontal line error: element has erroneous intersection.";
    errorsString[TError(7)] = "Action error: the element has 2 connections, there must be 1.";
    errorsString[TError(8)] = "Action error: the element has 0 connections, there must be 1.";
    errorsString[TError(9)] = "Action error: element has erroneous intersection.";
    errorsString[TError(10)] = "Action error: the element has an erroneous connection with a vertical line.";
    errorsString[TError(11)] = "Action error: the element has an erroneous connection with a horizontal line.";
    errorsString[TError(12)] = "Begin-end error: the element is not connected to a vertical line.";
    errorsString[TError(13)] = "Begin-end error: element has erroneous intersection.";
    errorsString[TError(14)] = "Begin-end error: the element has an erroneous connection with a vertical line.";
    errorsString[TError(15)] = "Begin-end error: the element has an erroneous connection with a horizontal line.";
    errorsString[TError(16)] = "Begin-end error: the diagram has an incorrect number of begin-end elements, there must be 2.";
    errorsString[TError(17)] = "If-else error: the element does not intersect with a vertical line or a horizontal line.";
    errorsString[TError(18)] = "If-else error: element has erroneous intersection.";
    errorsString[TError(19)] = "If-else error: the element has an erroneous connection with a vertical line or a horizontal line.";
}

void TDiagramsData::AddNewDiagram(QString n, QString p) {
    if (diagrams.contains(n) || tree.contains(n)) {
        return;
    }
    int maxId = 0;
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        if (it.value()->GetId() > maxId) {
            maxId = it.value()->GetId();
        }
    }
    maxId++;
    diagrams[n] = new TDiagram(maxId, n, "");
    if (tree.contains(p)) {
        tree[p].push_back(n);
    } else {
        tree[""].push_back(n);
    }
    emit DataChanged();
}

void TDiagramsData::AddNewFolder(QString n, QString p) {
    if (diagrams.contains(n) || tree.contains(n)) {
        return;
    }
    tree[n].resize(0);
    if (tree.contains(p)) {
        tree[p].push_back(n);
    } else {
        tree[""].push_back(n);
    }
    emit DataChanged();
}

void TDiagramsData::VerifyCurrent() {
    if (currentDiagram) {
        TError err = currentDiagram->Verify();
        if (err != Ok) {
            QMessageBox::information(nullptr, "Verify Info", ErrorToString(err, currentDiagram));
            return;
        }
    }
    QMessageBox::information(nullptr, "Verify Info", "Verify Ok.");
}

void TDiagramsData::VerifyAll() {
    QMap<QString, TDiagram*>::iterator it;
    for (it = diagrams.begin(); it != diagrams.end(); it++) {
        TError err = it.value()->Verify();
        if (err != Ok) {
            QMessageBox::information(nullptr, "Verify Info", ErrorToString(err, it.value()));
            return;
        }
    }
    QMessageBox::information(nullptr, "Verify Info", "Verify Ok.");
}


void TDiagramsData::NewFile() {
    ClearData();
    fileName = QFileDialog::getSaveFileName();
    QFile f(fileName);
    if(!f.open(QFile::Truncate | QFile::ReadWrite | QFile::Text)) {
        qDebug() << "Open file error";
        return;
    }
    f.close();
    file.NewFile(fileName, diagrams, tree, maxItemId);
    emit DataChanged();
}

void TDiagramsData::SaveFile() {
    if (fileName.isEmpty()) {
        return;
    }
    file.Save(fileName, diagrams, tree);
}

void TDiagramsData::OpenFile() {
    ClearData();
    fileName = QFileDialog::getOpenFileName();
    if (!fileName.endsWith(".drn")) {
        qDebug() << "Open file error";
        return;
    }
    file.Open(fileName, diagrams, tree, maxItemId);
    emit DataChanged();
}

void TDiagramsData::SetCurrentDiagram(QString name) {
    if (currentDiagram != diagrams.value(name) && !tree.contains(name)) {
        currentDiagram = diagrams[name];
        emit CurrentDiagramChanged();
    }
}

void TDiagramsData::ClearData() {
    diagrams.clear();
    tree.clear();
    currentDiagram = nullptr;
    emit DataRemoved();
}

