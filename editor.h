#pragma once

#include <QMainWindow>
#include <QDockWidget>
#include "codefield.h"
#include "data/diagramsdata.h"
#include "data/filemanager.h"
#include "diagramstree.h"
#include "textedit.h"

class TEditor : public QMainWindow
{
    Q_OBJECT

public:
    TEditor(QWidget *parent = 0);
    ~TEditor();

private slots:
    //void updateActions(const QItemSelection &selection);

    //edit menu slots
    void Undo();
    void Redo();
    void Cut();
    void Copy();
    void Paste();
    void Find();
    void SelectAll();
    void Delete();
    void DiagramDescription();
    void GotoDiagram();

    //insert menu slots
    void InsertDgrm();
    void InsertFolder();

    //drakon menu slots
    void VerifyCode();
    void VerifyAllCode();
    void BuildCode();
    void RunCode();

private:
    void CreateMenus();
    void CreateLayout();

private:
    TDiagramsData *data;
    TDiagramsTree *diagramsTree;
    TCodeField *codeField;
    QTextEdit *descriptionField;
    TTextEdit *insertDiagram;
    TTextEdit *insertFolder;

    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *insertMenu;
    QMenu *drakonMenu;

    //file menu actions
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *quitAct;

    //edit menu actions
    QAction *undoAct;
    QAction *redoAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *findAct;
    QAction *selectAllAct;
    QAction *deleteAct;
    QAction *diagramDescriptionAct;
    QAction *gotoDiagramAct;

    //insert menu actions
    QAction *insertDgrmAct;
    QAction *insertFolderAct;

    //drakon menu actions
    QAction *verifyAct;
    QAction *verifyAllAct;
    QAction *buildAct;
    QAction *runAct;
};
