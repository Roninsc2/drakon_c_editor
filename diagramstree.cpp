#include "diagramstree.h"
#include <QDebug>
#include <QString>

TDiagramsTree::TDiagramsTree(TDiagramsData *d, QWidget *parent)
    : QTreeWidget(parent), data(d)
{
    setHeaderHidden(true);
    connect(data, SIGNAL(DataRemoved()), this, SLOT(clear()));
    connect(data, SIGNAL(DataChanged()), this, SLOT(SetupTree()));
    connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)), this, SLOT(OnItemChanged(QTreeWidgetItem *, QTreeWidgetItem *)));
}

TDiagramsTree::~TDiagramsTree() {
}

void TDiagramsTree::DoUndo() {
}

void TDiagramsTree::SetupTree() {
    clear();
    QVector<QString> items = data->GetTreeItems();
    for (int i = 0; i < items.size(); i++) {
        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setText(0, items[i]);
        addTopLevelItem(item);
        if (data->GetTreeItemType(items[i])) { //if type == folder
            AddNextLevelItem(item, items[i]);
        }
    }
}

void TDiagramsTree::AddNextLevelItem(QTreeWidgetItem *parent, QString name) {
    parent->setExpanded(true);
    QVector<QString> items = data->GetTreeItems(name);
    for (int i = 0; i < items.size(); i++) {
        QTreeWidgetItem *item = new QTreeWidgetItem();
        item->setText(0, items[i]);
        parent->addChild(item);
        if (data->GetTreeItemType(items[i])) { //if type == folder
            AddNextLevelItem(item, items[i]);
        }
    }
}

void TDiagramsTree::OnItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous) {
    if (current && !current->childCount()) {
        data->SetCurrentDiagram(current->text(0));
    }
}

void TDiagramsTree::DoDelete() {
    if (hasFocus() && currentItem()) {
        QString delName = currentItem()->text(0);
        data->DeleteSelected(delName);
    }
}
